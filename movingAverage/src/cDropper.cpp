#include "../include/cDropper.hpp"
#include <stdio.h>

cDropper::cDropper(int threshold):
    m_threshold(threshold)
{

}

cDropper::~cDropper() {

}

/*! \brief the dropper check if a value is between a threshold
* the threshold is setted at the dropper instanciation
* return true if the value is in the threshold even false (to be dropped)
*/
bool cDropper::run(float newVal, float reference) {
    bool ret(false);

    if(((reference + m_threshold) > newVal) && ((reference - m_threshold) < newVal ))
        ret = true;
    
    return ret;
}



/* Only use for easily test
*/
int main() {
    float input[20] = {3.4, 3.2, 3.4, 7, 3.3, 3.32, 8, 3.12, 3.27, 0.2, 3.29, 3.28, 4, 4.5, 3.28, 3.4, 3.5, 3.25, 3.33, 3.28};

    cDropper dropper(1);

    
    for( int i=0; i < 20; i++) {
        printf("Value = %.2f \t dropper ret = %d \n", input[i], dropper.run(input[i], 3));
    }
}