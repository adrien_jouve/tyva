#include <stdio.h>
#include "../include/MovingAverage.hpp"


cMovingAverage::cMovingAverage( ):
    m_bufferReady(false),
    m_writeIndex(0),
    m_readIndex(0),
    m_average(0.0)
{
    m_buffer[m_sizeAverage] = {0};
}

/*! \brief the run method is where the average is calculated
*  the buffer management is done in this method
* return 0 if no error even a negative value (not managed for the moment)
*/
int cMovingAverage::run( float newVal ) {

    // Load the new value in our local buffer
    m_buffer[m_writeIndex] = newVal;
    m_writeIndex++;

    if(m_writeIndex == m_sizeAverage) {
        m_bufferReady = true;
        m_writeIndex = 0;
    }

    // Moving Average computation
    if(m_bufferReady) {
        float summ(0);

        for(int i=0; i < m_sizeAverage; i++) {
            summ += m_buffer[i];
        }

        set(summ/m_sizeAverage); 
    }

    return 0;
}
    


/* Only use for easily test
*/
int main () {

    // Test values
    float input[20] = {3.4, 3.2, 3.4, 7, 3.3, 3.32, 8, 3.12, 3.27, 3.28, 3.29, 3.28, 4, 3.5, 3.28, 3.4, 3.5, 3.25, 3.33, 3.28};

    // Our filter instantiation
    cMovingAverage average;

    printf(" ############  AVERAGE FILTERING  ############\n");
    printf("Start the filtring using static input data\n");

    // a loop to emulate AFE received value
    for( int i=0; i < sizeof(input)/sizeof(float); i++) {
        average.run(input[i]);
        printf("Interation %d the Average value is: %.4f\n", i, average.get());
    }

}