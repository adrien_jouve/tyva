#ifndef CMOVINGAVERAGE_HPP
#define CMOVINGAVERAGE_HPP

class cMovingAverage {
    public:
        cMovingAverage();

        int run( float newVal );

        float get(){ return m_average; };

    private:

        void set(float newAverage){ m_average = newAverage; };
        static const int m_sizeAverage = 5;
        float m_buffer[m_sizeAverage];
        bool m_bufferReady;
        int m_writeIndex;
        int m_readIndex;
        float m_average;
};
#endif /*CMOVINGAVERAGE_HPP*/