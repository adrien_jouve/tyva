#pragma once

class cDropper {

    public:
        cDropper(int threshold);
        ~cDropper();

        bool run(float newVal, float reference);

    private:
        int m_threshold;
};