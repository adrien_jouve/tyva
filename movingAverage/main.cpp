#include <stdio.h>
#include "MovingAverage.hpp"


float input[20] = {3.4, 3.2, 3.4, 7, 3.3, 3.32, 8, 3.12, 3.27, 3.28, 3.29, 3.28, 4, 3.5, 3.28, 3.4, 3.5, 3.25, 3.33, 3.28};

cMovingAverage ave;

int main (){


    printf(" ############  AVERAGE FILTERING  ############\n");
    printf("Start the filtring using static input data\n");

    for( int i=0; i < 20; i++) {
        ave.run(input[i]);
        printf("Value Average: %.4f", ave.get());
    }

}