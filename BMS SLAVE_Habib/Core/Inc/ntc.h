/*
 * ntc.h
 *
 *  Created on: 11 janv. 2019
 *      Author: habib
 */

#ifndef BMS_APP_NTC_H_
#define BMS_APP_NTC_H_

#include "analog.h"

#define CHANNEL_NTC0 2
#define CHANNEL_NTC1 0
#define CHANNEL_NTC2 4
#define CHANNEL_NTC3 3
#define CHANNEL_NTC4 5
#define CHANNEL_NTC5 7
#define CHANNEL_NTC6 8
#define CHANNEL_NTC7 1
#define CHANNEL_NTC8 6

typedef struct
{
	float r_ntc0;
	float r_ntc1;
	float r_ntc2;
	float r_ntc3;
	float r_ntc4;
	float r_ntc5;
	float r_ntc6;
	float r_ntc7;
	float r_ntc8;
	float counts;
} r_NTC_t;

typedef struct
{
	float temp_ntc0;
	float temp_ntc1;
	float temp_ntc2;
	float temp_ntc3;
	float temp_ntc4;
	float temp_ntc5;
	float temp_ntc6;
	float temp_ntc7;
	float temp_ntc8;
} temp_NTC_t;

void compute_ntc_temp(Vals_ADC_t *ptr);
void test_ntc(Vals_ADC_t *vals);


#endif /* BMS_APP_NTC_H_ */
