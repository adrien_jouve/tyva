/*
 * protocol.h
 *
 *  Created on: 19 déc. 2018
 *      Author: habib
 */

#ifndef SRC_PROTOCOL_H_
#define SRC_PROTOCOL_H_

#include "stm32l4xx_hal.h"


void CAN_Config(int sn);
uint8_t CAN_Transmit(uint32_t addr, uint32_t data_size, uint8_t *tab_data);
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan);

void test_can(void);


#endif /* SRC_PROTOCOL_H_ */
