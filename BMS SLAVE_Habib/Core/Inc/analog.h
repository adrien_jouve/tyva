/*
 * analog.h
 *
 *  Created on: 9 janv. 2019
 *      Author: habib
 */

#ifndef BMS_APP_ANALOG_H_
#define BMS_APP_ANALOG_H_

typedef struct
{
	int channel_cell1;
	int channel_cell2;
	int channel_voltage_pack;
	int channel_ntc0;
	int channel_ntc1;
	int channel_ntc2;
	int channel_ntc3;
	int channel_ntc4;
	int channel_ntc5;
	int channel_ntc6;
	int channel_ntc7;
	int channel_ntc8;
	int channel_vbat;
	int channel_dietemp;
} Vals_ADC_t;

Vals_ADC_t *transmit_samples(void);
Vals_ADC_t *test_ad(void);


#endif /* BMS_APP_ANALOG_H_ */
