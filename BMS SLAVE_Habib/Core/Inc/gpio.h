/**
  ******************************************************************************
  * File Name          : gpio.h
  * Description        : This file contains all the functions prototypes for 
  *                      the gpio  
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __gpio_H
#define __gpio_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

void MX_GPIO_Init(void);

/* USER CODE BEGIN Prototypes */
//void wakeupBQ(void);
//void wakeupBQdisable(void);
//void wakeupBQenable(void);

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN Private defines */
/* SOC LED Macro */
#define LED_SOC_OFF 	HAL_GPIO_WritePin(GPIOC, CMD_SOC_LED_Pin, GPIO_PIN_SET)
#define LED_SOC_ON 		HAL_GPIO_WritePin(GPIOC, CMD_SOC_LED_Pin, GPIO_PIN_RESET)

/* STATUS LED Macro */
#define LED_STATUS_OFF 	HAL_GPIO_WritePin(GPIOC, CMD_STATUS_LED_Pin, GPIO_PIN_SET)
#define LED_STATUS_ON 	HAL_GPIO_WritePin(GPIOC, CMD_STATUS_LED_Pin, GPIO_PIN_RESET)

/* ALL LEDS Macro */
#define ALL_LEDS_OFF 	HAL_GPIO_WritePin(GPIOC, CMD_STATUS_LED_Pin|CMD_SOC_LED_Pin, GPIO_PIN_SET)
#define ALL_LEDS_ON 	HAL_GPIO_WritePin(GPIOC, CMD_STATUS_LED_Pin|CMD_SOC_LED_Pin, GPIO_PIN_RESET)

/* TOOGLE LEDS Macro */
#define LED_SOC_TGL 	HAL_GPIO_TogglePin(GPIOC, CMD_SOC_LED_Pin)
#define LED_STATUS_TGL 	HAL_GPIO_TogglePin(GPIOC, CMD_STATUS_LED_Pin)

#define ALL_LEDS_TGL 	HAL_GPIO_TogglePin(GPIOC, CMD_STATUS_LED_Pin|CMD_SOC_LED_Pin)

#define WAKE_UP_BQ \
{\
	HAL_GPIO_WritePin(GPIOC, WAKEUP_BQ_Pin, GPIO_PIN_SET);\
	HAL_Delay(200);\
	HAL_GPIO_WritePin(GPIOC, WAKEUP_BQ_Pin, GPIO_PIN_RESET);\
	HAL_Delay(200);\
}

#define BALANCE_1S_ON 	HAL_GPIO_WritePin(GPIOB, P_BALANCE_CELL_0_Pin, GPIO_PIN_SET)
#define BALANCE_1S_OFF 	HAL_GPIO_WritePin(GPIOB, P_BALANCE_CELL_0_Pin, GPIO_PIN_RESET)

#define BALANCE_2S_ON 	HAL_GPIO_WritePin(GPIOB, P_BALANCE_CELL_1_Pin, GPIO_PIN_SET)
#define BALANCE_2S_OFF 	HAL_GPIO_WritePin(GPIOB, P_BALANCE_CELL_1_Pin, GPIO_PIN_RESET)


/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif
#endif /*__ pinoutConfig_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
