/*
 * ntc.c
 *
 *  Created on: 11 janv. 2019
 *      Author: habib
 */

#include <stdio.h>
#include <math.h>
#include "main.h"
#include "analog.h"
#include "ntc.h"

#define BETA_NTC 3900 /* 3435 */
#define ADC_RESOLUTION 4096
#define TEMPERATURENOMINAL 25
#define serialResistance 10000
#define nominalResistance 10000

r_NTC_t ad_ntc;
temp_NTC_t bmsTemperatures;

float steinhart;

/**
 *        V_supply ----\/\/\/---------|---------\/\/\/---- (Ground)
 *        							  |
 *              serialResistance      |       R_thermistor
 *                                    |
 *                              Analog Pin
 *
 *
 *                                    1
 *  T(°C) =  273 + ------------------------------------------------
 *                   1/T0 + 1/B ln(ADC_FullScale/ADC_measured - 1)
 *
 * */

void compute_ntc_temp(Vals_ADC_t *ptr)
{

	/* Compute moving average samples */
	ad_ntc.counts = 1;

	ad_ntc.r_ntc0 = ptr->channel_ntc0;
	ad_ntc.r_ntc0 = ADC_RESOLUTION / ad_ntc.r_ntc0 - 1;
	ad_ntc.r_ntc0 = serialResistance / ad_ntc.r_ntc0;
	/* (R/Ro) */
	steinhart = ad_ntc.r_ntc0 / nominalResistance ;
	/* ln(R/Ro) */
	steinhart = log(steinhart);
	/* 1/B * ln(R/Ro) */
	steinhart /= BETA_NTC;
	/* + (1/To) */
	steinhart += 1.0 / (TEMPERATURENOMINAL + 273.15);
	/* Invert */
	steinhart = 1.0 / steinhart;
	/* convert to C */
	steinhart -= 273.15;
	bmsTemperatures.temp_ntc0 = steinhart;

	ad_ntc.r_ntc1 = ptr->channel_ntc1;
	ad_ntc.r_ntc1 = ADC_RESOLUTION / ad_ntc.r_ntc1 - 1;
	ad_ntc.r_ntc1 = serialResistance / ad_ntc.r_ntc1;
	/* (R/Ro) */
	steinhart = ad_ntc.r_ntc1 / nominalResistance;
	/* ln(R/Ro) */
	steinhart = log(steinhart);
	/* 1/B * ln(R/Ro) */
	steinhart /= BETA_NTC;
	/* + (1/To) */
	steinhart += 1.0 / (TEMPERATURENOMINAL + 273.15);
	/* Invert */
	steinhart = 1.0 / steinhart;
	/* convert to C */
	steinhart -= 273.15;
	bmsTemperatures.temp_ntc1 = steinhart;

	ad_ntc.r_ntc2 = ptr->channel_ntc2;
	ad_ntc.r_ntc2 = ADC_RESOLUTION / ad_ntc.r_ntc2 - 1;
	ad_ntc.r_ntc2 = serialResistance / ad_ntc.r_ntc2;
	/* (R/Ro) */
	steinhart = ad_ntc.r_ntc2 / nominalResistance;
	/* ln(R/Ro) */
	steinhart = log(steinhart);
	/* 1/B * ln(R/Ro) */
	steinhart /= BETA_NTC;
	/* + (1/To) */
	steinhart += 1.0 / (TEMPERATURENOMINAL + 273.15);
	/* Invert */
	steinhart = 1.0 / steinhart;
	/* convert to C */
	steinhart -= 273.15;
	bmsTemperatures.temp_ntc2 = steinhart;

	ad_ntc.r_ntc3 = ptr->channel_ntc3;
	ad_ntc.r_ntc3 = ADC_RESOLUTION / ad_ntc.r_ntc3 - 1;
	ad_ntc.r_ntc3 = serialResistance / ad_ntc.r_ntc3;
	/* (R/Ro) */
	steinhart = ad_ntc.r_ntc3 / nominalResistance;
	/* ln(R/Ro) */
	steinhart = log(steinhart);
	/* 1/B * ln(R/Ro) */
	steinhart /= BETA_NTC;
	/* + (1/To) */
	steinhart += 1.0 / (TEMPERATURENOMINAL + 273.15);
	/* Invert */
	steinhart = 1.0 / steinhart;
	/* convert to C */
	steinhart -= 273.15;
	bmsTemperatures.temp_ntc3 = steinhart;

	ad_ntc.r_ntc4 = ptr->channel_ntc4;
	ad_ntc.r_ntc4 = ADC_RESOLUTION / ad_ntc.r_ntc4 - 1;
	ad_ntc.r_ntc4 = serialResistance / ad_ntc.r_ntc4;
	/* (R/Ro) */
	steinhart = ad_ntc.r_ntc4 / nominalResistance;
	/* ln(R/Ro) */
	steinhart = log(steinhart);
	/* 1/B * ln(R/Ro) */
	steinhart /= BETA_NTC;
	/* + (1/To) */
	steinhart += 1.0 / (TEMPERATURENOMINAL + 273.15);
	/* Invert */
	steinhart = 1.0 / steinhart;
	/* convert to C */
	steinhart -= 273.15;
	bmsTemperatures.temp_ntc4 = steinhart;

	ad_ntc.r_ntc5 = ptr->channel_ntc5;
	ad_ntc.r_ntc5 = ADC_RESOLUTION / ad_ntc.r_ntc5 - 1;
	ad_ntc.r_ntc5 = serialResistance / ad_ntc.r_ntc5;
	/* (R/Ro) */
	steinhart = ad_ntc.r_ntc5 / nominalResistance;
	/* ln(R/Ro) */
	steinhart = log(steinhart);
	/* 1/B * ln(R/Ro) */
	steinhart /= BETA_NTC;
	/* + (1/To) */
	steinhart += 1.0 / (TEMPERATURENOMINAL + 273.15);
	/* Invert */
	steinhart = 1.0 / steinhart;
	/* convert to C */
	steinhart -= 273.15;
	bmsTemperatures.temp_ntc5 = steinhart;

	ad_ntc.r_ntc6 = ptr->channel_ntc6;
	ad_ntc.r_ntc6 = ADC_RESOLUTION / ad_ntc.r_ntc6 - 1;
	ad_ntc.r_ntc6 = serialResistance / ad_ntc.r_ntc6;
	/* (R/Ro) */
	steinhart = ad_ntc.r_ntc6 / nominalResistance;
	/* ln(R/Ro) */
	steinhart = log(steinhart);
	/* 1/B * ln(R/Ro) */
	steinhart /= BETA_NTC;
	/* + (1/To) */
	steinhart += 1.0 / (TEMPERATURENOMINAL + 273.15);
	/* Invert */
	steinhart = 1.0 / steinhart;
	/* convert to C */
	steinhart -= 273.15;
	bmsTemperatures.temp_ntc6 = steinhart;

	ad_ntc.r_ntc7 = ptr->channel_ntc7;
	ad_ntc.r_ntc7 = ADC_RESOLUTION / ad_ntc.r_ntc7 - 1;
	ad_ntc.r_ntc7 = serialResistance / ad_ntc.r_ntc7;
	/* (R/Ro) */
	steinhart = ad_ntc.r_ntc7 / nominalResistance;
	/* ln(R/Ro) */
	steinhart = log(steinhart);
	/* 1/B * ln(R/Ro) */
	steinhart /= BETA_NTC;
	/* + (1/To) */
	steinhart += 1.0 / (TEMPERATURENOMINAL + 273.15);
	/* Invert */
	steinhart = 1.0 / steinhart;
	/* convert to C */
	steinhart -= 273.15;
	bmsTemperatures.temp_ntc7 = steinhart;

	ad_ntc.r_ntc8 = ptr->channel_ntc8;
	ad_ntc.r_ntc8 = ADC_RESOLUTION / ad_ntc.r_ntc8 - 1;
	ad_ntc.r_ntc8 = serialResistance / ad_ntc.r_ntc8;
	/* (R/Ro) */
	steinhart = ad_ntc.r_ntc8 / nominalResistance;
	/* ln(R/Ro) */
	steinhart = log(steinhart);
	/* 1/B * ln(R/Ro) */
	steinhart /= BETA_NTC;
	/* + (1/To) */
	steinhart += 1.0 / (TEMPERATURENOMINAL + 273.15);
	/* Invert */
	steinhart = 1.0 / steinhart;
	/* convert to C */
	steinhart -= 273.15;
	bmsTemperatures.temp_ntc8 = steinhart;

}


void test_ntc(Vals_ADC_t *vals)
{
	temp_NTC_t *ptr = &bmsTemperatures;

	compute_ntc_temp(vals);


#ifdef PRINTF_DEBUG
	printf("temperature ntc0 %f °C \r\n", ptr->temp_ntc0);
	printf("temperature ntc1 %f °C \r\n", ptr->temp_ntc1);
	printf("temperature ntc2 %f °C \r\n", ptr->temp_ntc2);
	printf("temperature ntc3 %f °C \r\n", ptr->temp_ntc3);
	printf("temperature ntc4 %f °C \r\n", ptr->temp_ntc4);
	printf("temperature ntc5 %f °C \r\n", ptr->temp_ntc5);
	printf("temperature ntc6 %f °C \r\n", ptr->temp_ntc6);
	printf("temperature ntc7 %f °C \r\n", ptr->temp_ntc7);
	printf("temperature ntc8 %f °C \r\n", ptr->temp_ntc8);
#endif
}
