/**
 ******************************************************************************
 * File Name          : analog.c
 * Description        : This file provides code for the configuration
 *                      of the ADC instances.
 ******************************************************************************
 ** This notice applies to any and all portions of this file
 * that are not between comment pairs USER CODE BEGIN and
 * USER CODE END. Other portions of this file, whether
 * inserted by the user or by software development tools
 * are owned by their respective copyright owners.
 *
 * COPYRIGHT(c) 2018 STMicroelectronics
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "adc.h"
#include "analog.h"
#include "gpio.h"

/* USER CODE BEGIN 0 */
#include <stdio.h>
/* USER CODE END 0 */

/* USER CODE BEGIN 1 */
Vals_ADC_t adc;


static void retrieve_adc(Vals_ADC_t *adc)
{

	/*
    PC0     ------> ADC1_IN1 ----> VCELL1
    PC1     ------> ADC1_IN2 ----> VCELL2
    PC2     ------> ADC1_IN3 ----> VOLTAGE PACK
    PC3     ------> ADC1_IN4 ----> NTC0
    PA1     ------> ADC1_IN6 ----> NTC2
    PA3     ------> ADC1_IN8 ----> NTC3
    PA4     ------> ADC1_IN9 ----> NTC4
    PA5     ------> ADC1_IN10 ---> NTC5
    PA6     ------> ADC1_IN11 ---> NTC6
    PA7     ------> ADC1_IN12 ---> NTC7
    PC4     ------> ADC1_IN13 ---> NTC1
    PB0     ------> ADC1_IN15 ---> NTC8
	 */

	HAL_ADC_Start(&hadc1);

	/* CELL1, CELL2, Voltage Pack, 9 NTC, Die Temperature and Vbat */
	HAL_ADC_PollForConversion(&hadc1, 100);
	adc->channel_cell1 = HAL_ADC_GetValue(&hadc1);

	HAL_ADC_PollForConversion(&hadc1, 100);
	adc->channel_cell2 = HAL_ADC_GetValue(&hadc1);

	HAL_ADC_PollForConversion(&hadc1, 100);
	adc->channel_voltage_pack = HAL_ADC_GetValue(&hadc1);

	HAL_ADC_PollForConversion(&hadc1, 100);
	adc->channel_ntc0 = HAL_ADC_GetValue(&hadc1);

	HAL_ADC_PollForConversion(&hadc1, 100);
	adc->channel_ntc1 = HAL_ADC_GetValue(&hadc1);

	HAL_ADC_PollForConversion(&hadc1, 100);
	adc->channel_ntc2 = HAL_ADC_GetValue(&hadc1);

	HAL_ADC_PollForConversion(&hadc1, 100);
	adc->channel_ntc3 = HAL_ADC_GetValue(&hadc1);

	HAL_ADC_PollForConversion(&hadc1, 100);
	adc->channel_ntc4 = HAL_ADC_GetValue(&hadc1);

	HAL_ADC_PollForConversion(&hadc1, 100);
	adc->channel_ntc5 = HAL_ADC_GetValue(&hadc1);

	HAL_ADC_PollForConversion(&hadc1, 100);
	adc->channel_ntc6 = HAL_ADC_GetValue(&hadc1);

	HAL_ADC_PollForConversion(&hadc1, 100);
	adc->channel_ntc7 = HAL_ADC_GetValue(&hadc1);

	HAL_ADC_PollForConversion(&hadc1, 100);
	adc->channel_ntc8 = HAL_ADC_GetValue(&hadc1);

	/* Die Temp channel */
	HAL_ADC_PollForConversion(&hadc1, 100);
	adc->channel_dietemp = HAL_ADC_GetValue(&hadc1);
	adc->channel_dietemp = __HAL_ADC_CALC_TEMPERATURE(VREF, adc->channel_dietemp, ADC_RESOLUTION_12B);


	/* µSTM32 Vbat pin voltage */
	HAL_ADC_PollForConversion(&hadc1, 100);
	adc->channel_vbat = HAL_ADC_GetValue(&hadc1);
	adc->channel_vbat = __HAL_ADC_CALC_VREFANALOG_VOLTAGE(adc->channel_vbat, ADC_RESOLUTION_12B);

	HAL_Delay (200);

	HAL_ADC_Stop (&hadc1);
}


/*
 *
 */
Vals_ADC_t *transmit_samples(void)
{
	retrieve_adc(&adc);
	return &adc;
}

Vals_ADC_t *test_ad(void)
{
	Vals_ADC_t *ptr = &adc;
	float cell2;
	int ii = 1;

	ptr = transmit_samples();

#ifdef PRINTF_DEBUG
	/* Cells Voltage */
#ifdef DEBUG_2CELLS
	printf("AD value Cell%d = %f\r\n",ii++, (ptr->channel_cell1) * BITAD * R_DIV_VCELL1);
	cell2 = (ptr->channel_cell2) * BITAD * R_DIV_VCELL2 - (ptr->channel_cell1) * BITAD * R_DIV_VCELL1;
	printf("AD value Cell%d = %f\r\n",ii, cell2);
	printf("AD value Voltage Pack = %d\r\n", ptr->channel_voltage_pack);
#endif


	/* ntc's temperatures */
	printf("AD value ntc%d = %d\r\n",ii++, ptr->channel_ntc0);
	printf("AD value ntc%d = %d\r\n",ii++, ptr->channel_ntc1);
	printf("AD value ntc%d = %d\r\n",ii++, ptr->channel_ntc2);
	printf("AD value ntc%d = %d\r\n",ii++, ptr->channel_ntc3);
	printf("AD value ntc%d = %d\r\n",ii++, ptr->channel_ntc4);
	printf("AD value ntc%d = %d\r\n",ii++, ptr->channel_ntc5);
	printf("AD value ntc%d = %d\r\n",ii++, ptr->channel_ntc6);
	printf("AD value ntc%d = %d\r\n",ii++, ptr->channel_ntc7);
	printf("AD value ntc%d = %d\r\n",ii++, ptr->channel_ntc8);

	/* stm32 Die Temp */
	printf("AD value Die Temp = %d\r\n", ptr->channel_dietemp);

	/* Vbat Voltage */
	printf("AD value Vbat = %d\r\n", ptr->channel_vbat);
#endif

	return ptr;
}
/* USER CODE END 1 */

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
