/*
 * bq769x0.c
 *
 *  Created on: 21 nov. 2018
 *      Author: habib
 */

#include "i2c.h"
#include "bq769x0.h"
/*=============================================================*/
/*                          VARIABLES                          */
/*=============================================================*/

/* Actual status of bq chip */
regSYS_STAT_t sys_stat;
bqTYPE_SIZE_t bq_typesize;
bqFRAME_I2C_t bq_frame;

/* BQ type */
int type;
uint8_t regValue = 1;

/* Open Circuit Voltage of cell for SOC 100%, 95%, ..., 5%, */
int *OCV;

/* indicates if a new current reading or an error is available from BMS IC */
int alertInterruptFlag;
int numberOfCells;                      		// number of cells allowed by IC
int connectedCells;                     		// actual number of cells connected
int cellVoltages[MAX_NUMBER_OF_CELLS];          // mV
int idCellMaxVoltage;
int idCellMinVoltage;
long batVoltage;                                // mV
long batCurrent;                                // mA

long nominalCapacity;    						// mAs, nominal capacity of battery pack, max. 1193 Ah possible
long coulombCounter;     						// mAs (= milli Coulombs) for current integration

/* Current limits (mA) */
long maxChargeCurrent;
long maxDischargeCurrent;
int idleCurrentThreshold; /* mA */

/* Cell voltage limits (mV) */
int maxCellVoltage;
int minCellVoltage;
int balancingMinCellVoltage_mV;
int balancingMaxVoltageDifference_mV;

/* uV/LSB */
int adcGain;

/* mV */
int adcOffset;

int errorStatus;
int autoBalancingEnabled;
unsigned int balancingStatus;     // holds on/off status of balancing switches
int balancingMinIdleTime_s;
unsigned long idleTimestamp;


/* Time out management */
static int ticks;
//static int timeout;

/*==================================================================*/
/*                LOCAL FUNCTIONS DECLARATIONS                      */
/*==================================================================*/
static int readRegister_bq(int address);
static void writeRegister_bq(int address, uint8_t *value);
static void updateVoltages(void);
static uint8_t crc8_ccitt_update(uint8_t inCrc, uint8_t inData);
static void wakeupBQdisable(void);
void wakeupBQenable (void);
/*==================================================================*/
/*                LOCAL FUNCTIONS IMPLEMENTATION                    */
/*==================================================================*/

/* USER CODE BEGIN 2 */
void wakeupBQdisable(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	/*Configure GPIO pin : PtPin */
	GPIO_InitStruct.Pin 	= WAKEUP_BQ_Pin;
	GPIO_InitStruct.Mode 	= GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull 	= GPIO_PULLDOWN;
	GPIO_InitStruct.Speed 	= GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(WAKEUP_BQ_GPIO_Port, &GPIO_InitStruct);
}

void wakeupBQenable(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	/*Configure GPIO pin : PtPin */
	GPIO_InitStruct.Pin 	= WAKEUP_BQ_Pin;
	GPIO_InitStruct.Mode 	= GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull 	= GPIO_NOPULL;
	HAL_GPIO_Init(WAKEUP_BQ_GPIO_Port, &GPIO_InitStruct);

	HAL_GPIO_WritePin(GPIOC, WAKEUP_BQ_Pin, GPIO_PIN_SET);
	HAL_Delay(200);
}


/**
 * @brief
 *
 * @param
 *
 * @retval
 */
int readRegister_bq(int reg_Address)
{
	uint8_t crc = 0;
	uint8_t buf[3];
	buf[0] = (char) reg_Address;

	HAL_I2C_Master_Transmit(&hi2c1, bq_frame.chipAddress << 1, buf, 1, 100);

	if (bq_typesize.crc_enable == true)
	{
		do {
			HAL_I2C_Master_Receive(&hi2c1, bq_frame.chipAddress << 1, buf, 2, 100);
			/* CRC is calculated over the slave address (including R/W bit) and data. */
			crc = crc8_ccitt_update(crc, (bq_frame.chipAddress << 1) | 1);
			crc = crc8_ccitt_update(crc, buf[0]);
		} while (crc != buf[1]);
		return buf[0];
	}
	else
	{
		HAL_I2C_Master_Receive(&hi2c1, bq_frame.chipAddress << 1, buf, 1, 100);
		return buf[0];
	}

}

/**
 * @brief
 *
 * @param
 *
 * @retval
 */
void writeRegister_bq(int reg_Address, uint8_t *value)
{
	uint8_t crc = 0;
	uint8_t buf[3];

	buf[0] = (uint8_t) reg_Address;
	buf[1] = *value;

	if (bq_typesize.crc_enable == true)
	{
		/* CRC is calculated over the slave address (including R/W bit), register address, and data. */
		crc = crc8_ccitt_update(crc, (bq_frame.chipAddress << 1) | 0);
		crc = crc8_ccitt_update(crc, buf[0]);
		crc = crc8_ccitt_update(crc, buf[1]);
		buf[2] = crc;

		HAL_I2C_Master_Transmit(&hi2c1, bq_frame.chipAddress << 1, buf, 3, 100);
	}
	else
	{
		HAL_I2C_Master_Transmit(&hi2c1, bq_frame.chipAddress << 1, buf, 2, 100);
	}
}

/**
 * @brief
 *
 * @param
 *
 * @retval
 */
uint8_t crc8_ccitt_update (uint8_t inCrc, uint8_t inData)
{
	uint8_t   i;
	uint8_t   data;

	data = inCrc ^ inData;

	for ( i = 0; i < 8; i++ )
	{
		if (( data & 0x80 ) != 0 )
		{
			data <<= 1;
			data ^= 0x07;
		}
		else
		{
			data <<= 1;
		}
	}
	return data;
}

/**
 *
 * @brief  Check the BQ chip type and crc feature enabled
 *
 * @param  void
 *
 * @retval bqTYPE_SIZE structure members Chip Address and crc feature
 */
void updateVoltages(void)
{
	long adcVal = 0;
	uint8_t buf[4];
	int connectedCellsTemp = 0;
	uint8_t crc;

	/* read cell voltages */
	buf[0] = (char) VC1_HI_BYTE;

	HAL_I2C_Master_Transmit(&hi2c1, bq_frame.chipAddress << 1, buf, 1, 0xFFFF);


	idCellMaxVoltage = 0;
	idCellMinVoltage = 0;

	for (int i = 0; i < numberOfCells; i++)
	{
		if (bq_typesize.crc_enable == true)
		{
			HAL_I2C_Master_Receive(&hi2c1, bq_frame.chipAddress << 1, buf, 4, 0xFFFF);

			adcVal = (buf[0] & 0b00111111) << 8 | buf[2];

			// CRC of first bytes includes slave address (including R/W bit) and data
			crc = crc8_ccitt_update(0, (bq_frame.chipAddress << 1) | 1);
			crc = crc8_ccitt_update(crc, buf[0]);
			if (crc != buf[1]) return; // don't save corrupted value

			// CRC of subsequent bytes contain only data
			crc = crc8_ccitt_update(0, buf[2]);
			if (crc != buf[3]) return; // don't save corrupted value
		}
		else
		{
			HAL_I2C_Master_Receive(&hi2c1, bq_frame.chipAddress << 1, buf, 2, 0xFFFF);
			adcVal = (buf[0] & 0b00111111) << 8 | buf[1];
		}

		cellVoltages[i] = adcVal * adcGain / 1000 + adcOffset;

		if (cellVoltages[i] > 500)
		{
			connectedCellsTemp++;
		}

		if (cellVoltages[i] > cellVoltages[idCellMaxVoltage])
		{
			idCellMaxVoltage = i;
		}

		if (cellVoltages[i] < cellVoltages[idCellMinVoltage] && cellVoltages[i] > 500)
		{
			idCellMinVoltage = i;
		}
	}
	connectedCells = connectedCellsTemp;

	/* read battery pack voltage */
	adcVal = (readRegister_bq(BAT_HI_BYTE) << 8) | readRegister_bq(BAT_LO_BYTE);
	batVoltage = 4.0 * adcGain * adcVal / 1000.0 + connectedCells * adcOffset;
	printf("Pack Voltage %ld\r\n", batVoltage);
	printf("Cell Voltage %d mV, %d mV, %d mV, %d mV, %d mV\r\n\r\n", cellVoltages[0], cellVoltages[1], cellVoltages[2],cellVoltages[3], cellVoltages[4] );
}


/*******************************************************************************************/

/*==================================================================*/
/*                         API FUNCTIONS                            */
/*==================================================================*/

/**
 *
 * @brief
 *
 * @param
 *
 * @retval
 */
void wakeupBQ(void)
{
	wakeupBQenable();
	HAL_Delay(200);
	wakeupBQdisable();
	HAL_Delay(200);
}


/**
 *
 * @brief
 *
 * @param
 *
 * @retval
 */
void balancing(void)
{
	printf("Balancing CELL1 0x%04x\r\n", regValue);
	writeRegister_bq(CELLBAL1, &regValue);
	regValue <<= 1;
	if (regValue >= 32)
		regValue = 1;

}

/**
 *
 * @brief  Check the BQ chip status errors flags and reset them if any
 *
 * @param  void
 *
 * @retval BQ status true or false
 */
bqSTAT_t CheckStatus_BQ(void)
{
	uint8_t regVal;

	/* Retrieve the BQ76xx status reg and proceed if I2C process is completed */
	sys_stat.regByte = readRegister_bq(SYS_STAT);

	/* Check up one time per second */
	if ((sys_stat.regByte & STAT_FLAGS) && (HAL_GetTick() - ticks > TIMEOUT1S))
	{
		ticks = HAL_GetTick();

		/* Clear XREADY Bit internal error chip */
		if (sys_stat.bits.DEVICE_XREADY)
		{
			regVal = STAT_DEVICE_XREADY;
			writeRegister_bq(SYS_STAT, &regVal);
			printf("Internal Fault ... clear it \r\n");
		}
		/* Alert error */
		if (sys_stat.regByte & STAT_OVRD_ALERT)
		{
			regVal = STAT_OVRD_ALERT;
			writeRegister_bq(SYS_STAT, &regVal);
			printf("Externally overdrive Alert ! \r\n");
		}
		/* Under Voltage error */
		if (sys_stat.regByte & STAT_UV)
		{
			updateVoltages();
			regVal = STAT_UV;
			if (cellVoltages[idCellMinVoltage] > minCellVoltage)
			{
				writeRegister_bq(SYS_STAT, &regVal);
				printf("UnderVoltage Cell %d ! \r\n", cellVoltages[idCellMinVoltage]);
			}
		}
		/* Over Voltage error */
		if (sys_stat.regByte & STAT_OV)
		{
			updateVoltages();
			if (cellVoltages[idCellMaxVoltage] < maxCellVoltage)
			{
				regVal = STAT_OV;
				writeRegister_bq(SYS_STAT, &regVal);
				printf("OverVoltage Cell %d ! \r\n", cellVoltages[idCellMinVoltage]);
			}
		}
	}
	else
	{
		return true;
	}

	/* Down there something went wrong in I2C process */
	//else
	//{
	/*
	 * HAL_ERROR
	 * HAL_BUSY
	 * HAL_TIMEOUT
	 */
	//return bq_Status;
	//}

	return true;
}


/**
 *
 * @brief  Check the BQ chip type and crc feature enabled
 *
 * @param  void
 *
 * @retval bqTYPE_SIZE structure members Chip Address and crc feature
 */
bqTYPE_SIZE_t* CheckAddressAndCrc(void)
{
	uint8_t regValue = 0x19;

	bq_frame.chipAddress = 0x08;
	bq_typesize.chipaddress = 0x08;
	bq_typesize.crc_enable = true;

	writeRegister_bq(CC_CFG, &regValue);
	if (readRegister_bq(CC_CFG) == 0x19)
	{
#ifdef BQ769X0_DEBUG
		printf("BQ I2C Address 0x08 and crc enabled\r\n");
#endif
		return &bq_typesize;
	}

	bq_frame.chipAddress = 0x18;
	bq_typesize.chipaddress = 0x18;
	bq_typesize.crc_enable = true;

	writeRegister_bq(CC_CFG, &regValue);
	if (readRegister_bq(CC_CFG) == 0x19)
	{
#ifdef BQ769X0_DEBUG
		printf("BQ I2C Address 0x18 and crc enabled\r\n");
#endif
		return &bq_typesize;
	}

	bq_frame.chipAddress = 0x08;
	bq_typesize.chipaddress = 0x08;
	bq_typesize.crc_enable = false;

	writeRegister_bq(CC_CFG, &regValue);
	if (readRegister_bq(CC_CFG) == 0x19)
	{
#ifdef BQ769X0_DEBUG
		printf("BQ I2C Address 0x18 and crc disabled\r\n");
#endif
		return &bq_typesize;
	}

	bq_frame.chipAddress = 0x18;
	bq_typesize.chipaddress = 0x18;
	bq_typesize.crc_enable = false;

	writeRegister_bq(CC_CFG, &regValue);
	if (readRegister_bq(CC_CFG) == 0x19)
	{
#ifdef BQ769X0_DEBUG
		printf("BQ I2C Address 0x08 and crc disabled\r\n");
#endif
		return &bq_typesize;
	}

	/* Here BQ chip is not responding ... */
	bq_frame.chipAddress = 0x00;
	bq_typesize.chipaddress = 0x00;
	bq_typesize.crc_enable = false;
	return &bq_typesize;
}


/**
 *
 * @brief  BQ and env Initialisation
 *
 * @param  void
 *
 * @retval void
 */
void initBQ_env(void)
{

	// Can't do the check like this !!
	type = bq76920;
	uint8_t regVal;

	if (type == bq76920)
	{
		numberOfCells = 5;
	}
	else if (type == bq76930)
	{
		numberOfCells = 10;
	}
	else
	{
		numberOfCells = 15;
	}

	/* initialize variables */
	for (int i = 0; i < numberOfCells - 1; i++)	// the latest value in array isn't initialized (you can add <= or remove -1)
	{
		cellVoltages[i] = 0;
	}

	if (CheckAddressAndCrc())
	{
		/**********************************
		 * initial settings for bq769x0
		 *********************************/

		/* switch ADC on, die temp source --> TSx_HI and TSx_LO */
		regVal = 0b00010000;
		writeRegister_bq(SYS_CTRL1, &regVal);

		/* get ADC offset and gain convert from 2's complement */
		adcOffset = (signed int) readRegister_bq(ADCOFFSET);
		adcGain = 365 + (((readRegister_bq(ADCGAIN1) & 0b00001100) << 1) |
				((readRegister_bq(ADCGAIN2) & 0b11100000) >> 5)); /* uV/LSB */
#ifdef BQ769X0_DEBUG
		printf("BQ Factory ADC Offset %d mV\r\n", adcOffset);
		printf("BQ Factory ADC Gain %d µV/LSB\r\n", adcGain);
#endif
	}
	else
	{
		/* TODO: do something else.b.. e.g. set error flag */
#ifdef BQ769X0_DEBUG
		printf("BMS communication error\n\r");
#endif
	}

}

void test_bq(void)
{
	CheckStatus_BQ();
	updateVoltages();
	balancing();
}
