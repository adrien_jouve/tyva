/*
 * protocol.c
 *
 *  Created on: 19 déc. 2018
 *      Author: habib
 */

#include <stdio.h>

#include "protocol.h"
#include "can.h"

uint32_t 				can_id;
CAN_FilterTypeDef  		sFilterConfig;
CAN_TxHeaderTypeDef		TxHeader;
CAN_RxHeaderTypeDef		RxHeader;
uint8_t					TxData[8];
uint8_t					RxData[8];
uint32_t				TxMailbox;

/**
 *
 * @brief
 *
 * @param
 *
 * @retval
 */
void CAN_Config(int sn)
{
	sFilterConfig.FilterBank = 0;
	sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
	sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
	sFilterConfig.FilterIdHigh = 0x320 << 5;
	sFilterConfig.FilterIdLow = 0;
	sFilterConfig.FilterMaskIdHigh = 0xFFF << 5;
	sFilterConfig.FilterMaskIdLow = 0;
	sFilterConfig.FilterFIFOAssignment = CAN_RX_FIFO0;
	sFilterConfig.FilterActivation = ENABLE;
	sFilterConfig.SlaveStartFilterBank = 14;

	if (HAL_CAN_ConfigFilter(&hcan1, &sFilterConfig) != HAL_OK)
	{
		/* Filter configuration Error */
		Error_Handler();
	}

	/* Start the CAN peripheral */
	if (HAL_CAN_Start(&hcan1) != HAL_OK)
	{
		/* Start Error */
		Error_Handler();
	}

	/* Activate CAN RX notification */
	if (HAL_CAN_ActivateNotification(&hcan1, CAN_IT_RX_FIFO0_MSG_PENDING) != HAL_OK)
	{
		/* Notification Error */
		Error_Handler();
	}

	TxHeader.StdId = 0x320;			/* Specify the remote CAN node address			*/
	TxHeader.ExtId = 0x01;			/* Not used here								*/
	TxHeader.RTR = CAN_RTR_DATA;	/* Data Frame type (Data or Commands)			*/
	TxHeader.IDE = CAN_ID_STD;		/* Standard frame but not Extended				*/
	TxHeader.DLC = 2;				/* Number of data size in bytes (from 0 to 8)*/
	TxHeader.TransmitGlobalTime = DISABLE;

	/* Assign ID CAN Node from the UID(96 bits) of the stm32l4 chip */
	can_id = sn & 0x00FF0000;
	can_id >>= 16;
	can_id |= 0x0300;
#ifdef PRINTF_DEBUG
	printf("can id 16 shifted : 0x%08lx\r\n", can_id);
#endif
}

/**
 *
 * @brief
 *
 * @param
 *
 * @retval
 */
uint8_t CAN_Transmit(uint32_t addr, uint32_t data_size, uint8_t *tab_data)
{
	TxHeader.StdId = addr;
	TxHeader.DLC = data_size;

	for(int i = 0; i < data_size; i++)
	{
		TxData[i] = tab_data[i];
	}

	if(HAL_CAN_AddTxMessage(&hcan1, &TxHeader, TxData, &TxMailbox) != HAL_OK)
	{
		/* Transmission request Error */
		Error_Handler();
	}

	return 1;
}

/**
 *
 * @brief
 *
 * @param  void
 *
 * @retval void
 */
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
	uint8_t ii;

	/* Process errors */
	if (HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO0, &RxHeader, RxData) != HAL_OK)
	{
		/* Reception Error */
		Error_Handler();
	}
	/* */
	else if ((RxHeader.StdId == 0x320) && (RxHeader.IDE == CAN_ID_STD) && (RxHeader.DLC == 2))
	{
		printf(" Address %#04x received !\n\r", (int)RxHeader.StdId);
		for (ii=0; ii<=TxHeader.DLC - 1; ii++)
		{
			printf(" Data Received %#04x" , RxData[0]);
		}
	}
}

void test_can(void)
{
	/* Set the data to be transmitted */
	TxData[0] = 0x55;
	TxData[1] = 0xAA;
	CAN_Transmit(0x320, sizeof(TxData), TxData);
}
