/*!
 ------------------------------------------------------------------------------
                   TYVA ENERGIE Technical Software Department
  ------------------------------------------------------------------------------
  Copyright (c) 2013-2015, TYVA ENERGIE.
  Address.
  All rights reserved.
  This source program is the property of TYVA ENERGIE Company mentioned above
  and may not be copied in any form or by any means, whether in part or in whole,
  except under license expressly granted by such TYVA ENERGIE company.
  All copies of this program, whether in part or in whole, and
  whether modified or not, must display this and all other
  embedded copyright and ownership notices in full.
  ------------------------------------------------------------------------------
 
  Project : BMS

  ------------------------------------------------------------------------------
  */

/**
******************************************************************************
* @file      cI2c.cpp
* @author    ajouve
* @date      14 f�vr. 2019
* @brief     Interface to use the I2C communication channel
* @details   
******************************************************************************
*/


#include "cI2c.hpp"

#ifdef UNITTEST
#include "Stubs/stm32_HAL_I2C_stubs.h"
#else
#include "stm32l4xx_hal.h"
#include "i2c.h"
#endif

#include <stdio.h>

//-------------------------------------------//
/// @fn cI2c( )
/// @brief cI2c constructor
//-------------------------------------------//
cI2c::cI2c(int i2cAddress):
    m_i2cAddress(i2cAddress),
    m_nbRetryCounterTransmit(0),
    m_nbRetryCounterReceive(0)
{
}

//-------------------------------------------//
/// @fn ~cI2c( )
/// @brief cI2c destructor
//-------------------------------------------//
cI2c::~cI2c(){
}


//-------------------------------------------//
/// @fn transmit( )
/// @brief wrapper and manager to send data over I2C (retry and error)
/// @param pData: pointer to data we want to send
/// @param size: the size of data want to send
/// @retval return 0 if ok, -1 if error
//-------------------------------------------//
// todo : should be protected by semaphore
int cI2c::transmit(uint8_t* pData, uint16_t size){

    int ret(0);
    HAL_StatusTypeDef HalStatus = HAL_I2C_Master_Transmit(&hi2c1, m_i2cAddress << 1, pData, 1, m_HALTimeoutCall);

    switch (HalStatus) {
        case HAL_OK:
            ret = 0;
            m_nbRetryCounterTransmit = 0;
            break;
        case HAL_ERROR:
            printf("cBq769x0::i2cTransmit: ERROR : HAL return Error when try transmit data over I2C to BQ");
            ret = -1;
            break;
        case HAL_BUSY:
        case HAL_TIMEOUT:
            printf("cBq769x0::i2cTransmit: ERROR : HAL return Busy or Timeout when try transmit data over I2C to BQ, retry");
            // Increment the counter to know how many retry is done
            m_nbRetryCounterTransmit ++;
            // Wait a delay before retry
            HAL_Delay(m_HALTimeoutCall);
            if (m_nbRetryCounterTransmit < m_maxRetry) {
                // Recall this method to retry
                ret = transmit(pData, size);
            }else {  // the end of the re-entrency
                printf("cBq769x0::i2cTransmit: ERROR : Impossible to communicate over I2C to BQ");
                ret = -1;
            }
            break;
        default:
            printf("cBq769x0::i2cTransmit: ERROR : Unknown error");
		break;

    }

    return ret;
}


//-------------------------------------------//
/// @fn receive( )
/// @brief wrapper and manager to the read function over I2C (retry and error)
/// @param pData: pointer to data we want to read
/// @param size: the size of data want to read
/// @retval return 0 if ok, -1 if error
//-------------------------------------------//
// todo : should be protected by semaphore
int cI2c::receive(uint8_t* pData, uint16_t size) {

    int ret(0);
    HAL_StatusTypeDef HalStatus = HAL_I2C_Master_Receive(&hi2c1, m_i2cAddress << 1, pData, size, m_HALTimeoutCall);

    switch (HalStatus) {
        case HAL_OK:
            ret = 0;
            m_nbRetryCounterReceive = 0;
            break;
        case HAL_ERROR:
            printf("cBq769x0::i2cReceive: ERROR : HAL return Error when try receive data over I2C to BQ");
            ret = -1;
            break;
        case HAL_BUSY:
        case HAL_TIMEOUT:
            printf("cBq769x0::i2cReceive: ERROR : HAL return Busy or Timeout when try receive data over I2C to BQ, retry");
            // Increment the counter to know how many retry is done
            m_nbRetryCounterReceive ++;
            // Wait a delay before retry
            HAL_Delay(m_HALTimeoutCall);
            if (m_nbRetryCounterReceive < m_maxRetry) {
                // Recall this method to retry
                ret = receive(pData, size);
            }else {  // the end of the re-entrency
                printf("cBq769x0::i2cReceive: ERROR : Impossible to communicate over I2C to BQ");
                ret = -1;
            }
            break;
        default:
            printf("cBq769x0::i2cReceive: ERROR : Unknown error");
		break;
    }

    return ret;
}



