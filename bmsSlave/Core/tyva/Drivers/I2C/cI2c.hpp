/*!
 ------------------------------------------------------------------------------
                   TYVA ENERGIE Technical Software Department
  ------------------------------------------------------------------------------
  Copyright (c) 2013-2015, TYVA ENERGIE.
  Address.
  All rights reserved.
  This source program is the property of TYVA ENERGIE Company mentioned above
  and may not be copied in any form or by any means, whether in part or in whole,
  except under license expressly granted by such TYVA ENERGIE company.
  All copies of this program, whether in part or in whole, and
  whether modified or not, must display this and all other
  embedded copyright and ownership notices in full.
  ------------------------------------------------------------------------------
 
  Project : BMS

  ------------------------------------------------------------------------------
  */

/**
******************************************************************************
* @file      cI2c.hpp
* @author    ajouve
* @date      14 f�vr. 2019
* @brief     Interface to use the I2C communication channel
* @details   
******************************************************************************
*/


#ifndef TYVA_DRIVERS_I2C_CI2C_HPP_
#define TYVA_DRIVERS_I2C_CI2C_HPP_

#ifdef UNITTEST
#include "Stubs/stm32_stubs.h"
#else
#include "stm32l431xx.h"
#endif

class cI2c {
	//----------------------------- Public Members ---------------------------//
    public:
		cI2c(int i2cAddress);
		~cI2c();

		//-------------------------------------------//
		/// @fn transmit( )
		/// @brief wrapper and manager to send data over I2C (retry and error)
		/// @param pData: pointer to data we want to send
		/// @param size: the size of data want to send
		/// @retval return 0 if ok, -1 if error
		//-------------------------------------------//
		int transmit(uint8_t* pData, uint16_t size);

		//-------------------------------------------//
		/// @fn receive( )
		/// @brief wrapper and manager to the read function over I2C (retry and error)
		/// @param pData: pointer to data we want to read
		/// @param size: the size of data want to read
		/// @retval return 0 if ok, -1 if error
		//-------------------------------------------//
		int receive(uint8_t* pData, uint16_t size);

	//---------------------------- Protected Members -------------------------//
    protected:

        // BQ I2C bus address, find in the datasheet (all TI BQ have a specific I2C address set in production)
        int m_i2cAddress;

        // The maximum number of retry when call I2C receive/transmit method
        static int const m_maxRetry = 5;

        // Index to counter the number of retry when call I2C receive/transmit method
        int m_nbRetryCounterTransmit;
        int m_nbRetryCounterReceive;

        // The timeout for all the HAL function calling
        static int const m_HALTimeoutCall = 100;

};

#endif // TYVA_DRIVERS_I2C_CI2C_HPP_
