/*!
 ------------------------------------------------------------------------------
                   TYVA ENERGIE Technical Software Department
  ------------------------------------------------------------------------------
  Copyright (c) 2013-2015, TYVA ENERGIE.
  Address.
  All rights reserved.
  This source program is the property of TYVA ENERGIE Company mentioned above
  and may not be copied in any form or by any means, whether in part or in whole,
  except under license expressly granted by such TYVA ENERGIE company.
  All copies of this program, whether in part or in whole, and
  whether modified or not, must display this and all other
  embedded copyright and ownership notices in full.
  ------------------------------------------------------------------------------

  Project : BMS

  ------------------------------------------------------------------------------
  */

/**
******************************************************************************
* @file      cHalError.hpp
* @author    ajouve
* @date      18 f�vr. 2019
* @brief     Error management class
* @details
******************************************************************************
*/

#ifndef TYVA_DRIVERS_HALERROR_CHALERROR_HPP_
#define TYVA_DRIVERS_HALERROR_CHALERROR_HPP_

#include "stm32l431xx.h"

class cHalError {
	//----------------------------- Public Members ---------------------------//
    public:
		cHalError(){};
		~cHalError(){};

        //-------------------------------------------//
        /// @fn init( )
        /// @brief to initialized the driver
        //-------------------------------------------//
        virtual int init( void ){};

		//-------------------------------------------//
		/// @fn run( )
		/// @brief show the error to the console
		/// @param error: the error
		/// @retval return 0 if ok, -1 if error
		//-------------------------------------------//
		int run(HAL_StatusTypeDef error){};


};

#endif // TYVA_DRIVERS_HALERROR_CHALERROR_HPP_
