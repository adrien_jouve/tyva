/*!
 ------------------------------------------------------------------------------
                   TYVA ENERGIE Technical Software Department
  ------------------------------------------------------------------------------
  Copyright (c) 2013-2015, TYVA ENERGIE.
  Address.
  All rights reserved.
  This source program is the property of TYVA ENERGIE Company mentioned above
  and may not be copied in any form or by any means, whether in part or in whole,
  except under license expressly granted by such TYVA ENERGIE company.
  All copies of this program, whether in part or in whole, and
  whether modified or not, must display this and all other
  embedded copyright and ownership notices in full.
  ------------------------------------------------------------------------------
 
  Project : BMS
  Module  : measure

  ------------------------------------------------------------------------------
  */

/**
******************************************************************************
* @file      cCan.hpp
* @author    Adrien Jouve
* @date      January 15, 2019
* @brief     interface to access to CAN communication layer
* @details   
******************************************************************************
*/
#ifndef _cCan_
#define _cCan_

#include "Drivers/cMasterComm.hpp"

#ifndef UNITTEST
#include "stm32l431xx.h"
#include "stm32l4xx_hal.h"
#endif

#define CAN_DEBUG

class cCan: public cMasterComm
{
    public:
        //-------------------------------------------//
        /// @fn cCan( )
        /// @brief cCan constructor
        //-------------------------------------------//
        cCan(uint32_t masterAddress);
        ~cCan(){};

        //-------------------------------------------//
        /// @fn init( )
        /// @brief to initialized CAN peripheral
        //-------------------------------------------//
        virtual int init( void );

        //-------------------------------------------//
		/// @fn isDataAvailable( )
		/// @brief return true a new data is available even false
		//-------------------------------------------//
        bool isDataAvailable() { return m_isDataAvailable; };

        //-------------------------------------------//
		/// @fn getDataSize( )
		/// @brief return the size of available data
		//-------------------------------------------//
        uint32_t getSize() { return m_rxHeader.DLC; };
        uint8_t* get() {
        	m_isDataAvailable = false;
        	return m_rxData + 2;
        };

        //-------------------------------------------//
        /// @fn receive( )
        /// @brief receive data from the CAN
        //-------------------------------------------//
        void receive();

        //-------------------------------------------//
        /// @fn transmit( )
        /// @brief send data to the master over CAN
        /// @param pData: pointer to data we want to send
        /// @param size: the size of data unit32 (number of Unit32)
        /// @retval return 0 if ok, -1 if error
        //-------------------------------------------//
        virtual int transmit(uint32_t *pData, uint32_t size);

        //-------------------------------------------//
        /// @fn transmit( )
        /// @brief send data to the master over CAN
        /// @param pData: pointer to data we want to send
        /// @param size: the size of data in bytes
        /// @retval return 0 if ok, -1 if error
        //-------------------------------------------//
        virtual int transmit(uint8_t *pData, uint32_t size);

    protected:

        //-------------------------------------------//
        /// @fn getUniqueId( )
        /// @brief update the voltage value to get data from BQ
        /// @retVal return the a unique ID for this processor
        //-------------------------------------------//
        uint32_t getUniqueId();

        // Master Address
        uint32_t m_masterAddress;

        // Transmit Header
        CAN_TxHeaderTypeDef     m_txHeader;

        // Receive Header
        CAN_RxHeaderTypeDef     m_rxHeader;

        // Receive data buffer
        uint8_t m_rxData[8];	// Size = 8 because we read 8 bytes per 8 bytes

        // The maximum number of retry when call I2C receive/transmit method
        static int const m_maxRetry = 5;

        // Index to counter the number of retry when call I2C receive/transmit method
        int m_nbRetryCounterTransmit;
        int m_nbRetryCounterReceive;

        // The timeout for all the HAL function calling
        static int const m_HALTimeoutCall = 100;

        // true if data available even false
        bool m_isDataAvailable;

};
#endif //_cCan_
