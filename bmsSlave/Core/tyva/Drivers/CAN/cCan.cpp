/*!
 ------------------------------------------------------------------------------
                   TYVA ENERGIE Technical Software Department
  ------------------------------------------------------------------------------
  Copyright (c) 2013-2015, TYVA ENERGIE.
  Address.
  All rights reserved.
  This source program is the property of TYVA ENERGIE Company mentioned above
  and may not be copied in any form or by any means, whether in part or in whole,
  except under license expressly granted by such TYVA ENERGIE company.
  All copies of this program, whether in part or in whole, and
  whether modified or not, must display this and all other
  embedded copyright and ownership notices in full.
  ------------------------------------------------------------------------------
 
  Project : BMS
  Module  : measure

  ------------------------------------------------------------------------------
  */

/**
******************************************************************************
* @file      cCan.cpp
* @author    Adrien Jouve
* @date      January 15, 2019
* @brief     interface to access to CAN communication layer
* @details   
******************************************************************************
*/
// todo : need to manage CAN bus address for the slave device (how to do that?)

#include "cCan.hpp"

#include <stdlib.h>

#ifdef UNITTEST
#include "Stubs/stm32_HAL_CAN_stubs.h"
#else
	#include "can.h"
#endif

//-------------------------------------------//
/// @fn cCan( )
/// @brief cCan constructor
//-------------------------------------------//
cCan::cCan( uint32_t masterAddress ):
	m_masterAddress(masterAddress),
	m_nbRetryCounterTransmit(0),
	m_nbRetryCounterReceive(0),
	m_isDataAvailable(false)
{
}


//-------------------------------------------//
/// @fn init( )
/// @brief to initialized CAN peripheral
//-------------------------------------------//
int cCan::init( void ){

    CAN_FilterTypeDef sFilterConfig;
    uint32_t canId(0);

	sFilterConfig.FilterBank = 0;
	sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
	sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
	sFilterConfig.FilterIdHigh = 0x320 << 5;
	sFilterConfig.FilterIdLow = 0;
	sFilterConfig.FilterMaskIdHigh = 0xFFF << 5;
	sFilterConfig.FilterMaskIdLow = 0;
	sFilterConfig.FilterFIFOAssignment = CAN_RX_FIFO0;
	sFilterConfig.FilterActivation = ENABLE;
	sFilterConfig.SlaveStartFilterBank = 14;

	if (HAL_CAN_ConfigFilter(&hcan1, &sFilterConfig) != HAL_OK)
	{
		// Filter configuration Error
		Error_Handler();
	}

	// Start the CAN peripheral
	if (HAL_CAN_Start(&hcan1) != HAL_OK)
	{
		// Start Error
		Error_Handler();
	}

	// Activate CAN RX notification
	if (HAL_CAN_ActivateNotification(&hcan1, CAN_IT_RX_FIFO0_MSG_PENDING) != HAL_OK)
	{
		// Notification Error
		Error_Handler();
	}

	// Set the Transmit header
	m_txHeader.StdId = 0x320;			// Specify the remote CAN node address
	m_txHeader.ExtId = 0x01;			// Not used here
	m_txHeader.RTR = CAN_RTR_DATA;	    // Data Frame type (Data or Commands)
	m_txHeader.IDE = CAN_ID_STD;		// Standard frame but not Extended
	m_txHeader.DLC = 2;				    // Number of data size in bytes (from 0 to 8)
	m_txHeader.TransmitGlobalTime = DISABLE;

	// Assign ID CAN Node from the UID(96 bits) of the stm32l4 chip
	canId = getUniqueId() & 0x00FF0000;
	canId >>= 16;
	canId |= 0x0300;
#ifdef CAN_DEBUG
	printf("cCan::init : DEBUG : can id = 0x%08lx\n", canId);
#endif

	return 0;
}


//-------------------------------------------//
/// @fn receive( )
/// @brief receive data from the CAN
//-------------------------------------------//
// todo : needs to be refactored
void cCan::receive() {

    // todo : need to manage error
    if (HAL_CAN_GetRxMessage(&hcan1, CAN_RX_FIFO0, &m_rxHeader, m_rxData) != HAL_OK) {
        // Reception Error
    	printf("cCan::receive: ERROR : HAL return Error when try received data over CAN from the MASTER");
    }

    else if ((m_rxHeader.StdId == m_masterAddress) && (m_rxHeader.IDE == CAN_ID_STD) && (m_rxHeader.DLC >= 2)) {
#ifdef CAN_DEBUG
        printf("cCan::receive : DEBUG : Address %#04x received !\n\r", (int)m_rxHeader.StdId);
        for (uint8_t i = 0; i <= m_txHeader.DLC - 1; i++) {
            printf("cCan::receive : DEBUG : Data Received %#04x" , m_rxData[i]);
        }
#endif
		m_isDataAvailable = true;
    }
}


//-------------------------------------------//
/// @fn transmit( )
/// @brief send data to the master over CAN
/// @param pData: pointer to data we want to send
/// @param size: the size of data in unit32
/// @retval return 0 if ok, -1 if error
//-------------------------------------------//
// todo : do unit test on this algorithm
int cCan::transmit(uint32_t *pData, uint32_t size) {

	int ret(0);

    // change the size from number of uint32 to number of uint8
	size = size*4;

	uint8_t *array8Data = (uint8_t *)malloc(size);
	//uint8_t array8Data[size];

	// Convert uint32 array to uint8 array (|LSB|1|2|MSB|)
	int indexIn(0);
	for (uint32_t i = 0; i < size; i++) {
		// each 4 bytes increment the input array
		if (i%4 == 0 && i != 0)
			indexIn++; 

		array8Data[i] = (uint8_t)(pData[indexIn] >> 8 * (3 - (i- 4*indexIn)));
	}

	ret = transmit(array8Data, size);

	free(array8Data);
	array8Data = NULL;

	return ret;
}
//-------------------------------------------//
/// @fn transmit( )
/// @brief send data to the master over CAN
/// @param pData: pointer to data we want to send
/// @param size: the size of data in bytes
/// @retval return 0 if ok, -1 if error
//-------------------------------------------//
int cCan::transmit(uint8_t *pData, uint32_t size) {

    int ret(0);
	uint8_t txData[8];
    uint32_t txMailbox;

    m_txHeader.StdId = m_masterAddress;
    m_txHeader.DLC = size;

    // todo Need to be refactored because, I think, that's use less to localy copy the pDatat pointer
    for(uint32_t i = 0; i < size; i++)
    {
        txData[i] = pData[i];
    }

    // todo : create a generic class to manage all protocol error for I2C and CAN
    HAL_StatusTypeDef HalStatus = HAL_CAN_AddTxMessage(&hcan1, &m_txHeader, txData, &txMailbox);

	switch (HalStatus) {
		case HAL_OK:
			ret = 0;
			m_nbRetryCounterTransmit = 0;
			break;
		case HAL_ERROR:
			printf("cCan::transmit: ERROR : HAL return Error when try transmit data over CAN to the MASTER");
			ret = -1;
			break;
		case HAL_BUSY:
		case HAL_TIMEOUT:
			printf("cCan::transmit: ERROR : HAL return Busy or Timeout when try transmit data over CAN to the MASTER, retry");
			// Increment the counter to know how many retry is done
			m_nbRetryCounterTransmit ++;
			// Wait a delay before retry
			HAL_Delay(m_HALTimeoutCall);
			if (m_nbRetryCounterTransmit < m_maxRetry) {
				// Recall this method to retry
				ret = transmit(pData, size);
			}else {  // the end of the re-entrency
				printf("cCan::transmit: ERROR : Impossible to communicate over CAN to the MASTER");
				ret = -1;
			}
		break;
		default:
			printf("cCan::transmit: ERROR : Unknown error");
		break;
	}

    return ret;
}


//-------------------------------------------//
/// @fn getUniqueId( )
/// @brief update the voltage value to get data from BQ
/// @retVal return the a unique ID for this processor
//-------------------------------------------//
uint32_t cCan::getUniqueId() {

    uint32_t serialNumber(0);

    serialNumber = HAL_GetUIDw0();
#ifdef CAN_DEBUG
    printf("cCan::getUniqueId : DEBUG : serial number = 0x%08lx\n", serialNumber);
#endif

    return serialNumber;
}

