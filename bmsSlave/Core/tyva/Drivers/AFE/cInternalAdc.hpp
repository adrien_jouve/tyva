/*!
 ------------------------------------------------------------------------------
                   TYVA ENERGIE Technical Software Department
  ------------------------------------------------------------------------------
  Copyright (c) 2013-2015, TYVA ENERGIE.
  Address.
  All rights reserved.
  This source program is the property of TYVA ENERGIE Company mentioned above
  and may not be copied in any form or by any means, whether in part or in whole,
  except under license expressly granted by such TYVA ENERGIE company.
  All copies of this program, whether in part or in whole, and
  whether modified or not, must display this and all other
  embedded copyright and ownership notices in full.
  ------------------------------------------------------------------------------
 
  Project : BMS

  ------------------------------------------------------------------------------
  */

/**
******************************************************************************
* @file      cInternalAdc.hpp
* @author    ajouve
* @date      19 f�vr. 2019
* @brief     
* @details   
******************************************************************************
*/


#ifndef TYVA_DRIVERS_AFE_CINTERNALADC_HPP_
#define TYVA_DRIVERS_AFE_CINTERNALADC_HPP_

#include "cAfe.hpp"


class cInternalAdc : public cAfe {
	//----------------------------- Public Members ---------------------------//
    public:
		cInternalAdc(){};
		~cInternalAdc(){};

        //-------------------------------------------//
        /// @fn init( )
        /// @brief to initialized the AFE peripheral
        //-------------------------------------------//
        virtual int init( void ){ return 0; };

        //-------------------------------------------//
        /// @fn update( )
        /// @brief access to the AFE to get all values
        //-------------------------------------------//
        virtual int update( void ){ return 0; };

        //-------------------------------------------//
        /// @fn get( )
        /// @param information : from enum in the sub-class (cBq769x0) contain requested information
        /// @brief get a specific value read by AFE
        //-------------------------------------------//
        virtual unsigned long get( tGetData information ){ return 0; };

        //-------------------------------------------//
        /// @fn getCellVoltage( )
        /// @param channel : requested channel
        /// @brief get cell voltage value
        //-------------------------------------------//
        virtual unsigned long getCellVoltage( int channel ){ return 0; };

        //-------------------------------------------//
        /// @fn getNumberOfCells( )
        /// @brief return the number of cells
        //-------------------------------------------//
        virtual uint32_t getNumberOfCells( void ){ return 0; };

        //-------------------------------------------//
        /// @fn balance( )
        /// @brief balance, so short circuit a cell
        //-------------------------------------------//
        virtual void balance( tBalance request ){};

	//---------------------------- Protected Members -------------------------//
    protected:

};

#endif // TYVA_DRIVERS_AFE_CINTERNALADC_HPP_
