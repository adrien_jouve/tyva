/*!
 ------------------------------------------------------------------------------
                   TYVA ENERGIE Technical Software Department
  ------------------------------------------------------------------------------
  Copyright (c) 2013-2015, TYVA ENERGIE.
  Address.
  All rights reserved.
  This source program is the property of TYVA ENERGIE Company mentioned above
  and may not be copied in any form or by any means, whether in part or in whole,
  except under license expressly granted by such TYVA ENERGIE company.
  All copies of this program, whether in part or in whole, and
  whether modified or not, must display this and all other
  embedded copyright and ownership notices in full.
  ------------------------------------------------------------------------------
 
  Project : BMS
  Module  : measure

  ------------------------------------------------------------------------------
  */

/**
******************************************************************************
* @file      cBq769x0.cpp
* @author    Adrien Jouve
* @date      February 5, 2019
* @brief     interface to access to the BQ sensor
* @details   
******************************************************************************
*/


#include "cBq769x0.hpp"
#include <stdlib.h>

#ifdef UNITTEST
#include "Stubs/stm32_HAL_stubs.h"
#endif


//-------------------------------------------//
/// @fn cBq769x0( )
/// @brief cBq769x0 constructor
//-------------------------------------------//
cBq769x0::cBq769x0( GPIO_TypeDef *BqPort,
                    int BqPin,
                    tTypes type,
					int minCellVal,
					int maxCellVal):
    m_bqPort(BqPort),
    m_bqPinNumber(BqPin),
    m_bqType(type),
	m_minCellValue(minCellVal),
	m_maxCellValue(maxCellVal)
{
    m_timeLastSanity = HAL_GetTick();
    m_crc = NULL;
    m_i2c = NULL;
}


cBq769x0::~cBq769x0(){
    if (m_crc != NULL) {
        delete(m_crc);
        m_crc = NULL;
    }
    if (m_i2c != NULL) {
        delete(m_i2c);
        m_i2c = NULL;
    }
}

//-------------------------------------------//
/// @fn wakeupBqDisable( )
/// @brief to Disable the wakeup the BQ peripheral
//-------------------------------------------//
// todo : is those methods use full ?
void cBq769x0::wakeupBqDisable( )
{
 	GPIO_InitTypeDef GPIO_InitStruct = {0};

	// Configure GPIO pin : PtPin
	GPIO_InitStruct.Pin 	= m_bqPinNumber;
	GPIO_InitStruct.Mode 	= GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull 	= GPIO_PULLDOWN;
	GPIO_InitStruct.Speed 	= GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(m_bqPort, &GPIO_InitStruct);
}


//-------------------------------------------//
/// @fn wakeupBqEnable( )
/// @brief to Enable the wakeup the BQ peripheral
//-------------------------------------------//
// todo : is those methods use full ?
void cBq769x0::wakeupBqEnable( )
{
 	GPIO_InitTypeDef GPIO_InitStruct = {0};

	// Configure GPIO pin : PtPin
	GPIO_InitStruct.Pin 	= m_bqPinNumber;
	GPIO_InitStruct.Mode 	= GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull 	= GPIO_NOPULL;
	HAL_GPIO_Init(m_bqPort, &GPIO_InitStruct);

	HAL_GPIO_WritePin(m_bqPort, m_bqPinNumber, GPIO_PIN_SET);
	HAL_Delay(200);
}


//-------------------------------------------//
/// @fn init( )
/// @brief to initialized the AFE peripheral
/// @retval return 0 if all ok, -1 if error, -2 if use external ADC
//-------------------------------------------//
int cBq769x0::init( ) {

    int ret (-1);

    // todo : find another way to establish the BQ type
    switch (m_bqType) {
        case eBq76920:
            m_numberOfCells = 5;
            break;
        case eBq76930:
            m_numberOfCells = 10;
            break;
        case eBq76940:
            m_numberOfCells = 15;
            break;
        case eInternalADC:
#ifdef BQ769X0_DEBUG
            printf("cBq769x0::init : DEBUG : Internal ADC detected\n");
#endif
            m_numberOfCells = 3;
            return -2;
            break;
        default:
            printf("cBq769x0::init : ERROR : not managed type of BQ");
            return -1;
            break;
    }

	// initialize variables
    for (int i = 0; i < m_numberOfCells - 1; i++) {
		m_cellVoltages[i] = 0;
	}

    ret = findBqConfig();

	if (ret == 0)
	{
	    // Instantiate the I2C object to communicate with the AFE
	    if (m_i2c == NULL) {
	        m_i2c = new cI2c(m_i2cAddress);
	    }

 		//*******************************
		// initial settings for bq769x0
		//*******************************

		// switch ADC on, die temp source --> TSx_HI and TSx_LO
		ret = write(SYS_CTRL1, 0b00010000);
        // Get ADC offset value
        ret &= read(ADCOFFSET, 1, &m_adcOffset);
        // Get ADC gain value
        uint8_t adcGain[2] = {0,0};
        ret &= read(ADCGAIN1, 1, adcGain);
        ret &= read(ADCGAIN2, 1, adcGain + 1);

        if (ret == 0) {
            // ADC gain calculation convert from 2's complement
            m_adcGain = 365 + ((adcGain[0] & 0b00001100) << 1) + ((adcGain[1] & 0b11100000) >> 5); // uV/LSB
#ifdef BQ769X0_DEBUG
            printf("cBq769x0::init : DEBUG : BQ Factory ADC Offset %d mV\r\n", m_adcOffset);
            printf("cBq769x0::init : DEBUG : BQ Factory ADC Gain %d uV/LSB\r\n", m_adcGain);
#endif
        }
	}
    
#ifdef BQ769X0_DEBUG
    if (ret != 0) {
		// TODO: do something else.b.. e.g. set error flag
		printf("cBq769x0::init : ERROR : AFE communication error, STOP the process\n\r");
	}
#endif

    return ret;
}


//-------------------------------------------//
/// @fn update( )
/// @brief access to the AFE to get all values
/// @retval return 0 if all ok, -1 if error
//-------------------------------------------//
int cBq769x0::update( void ) {

    int ret(-1);

    // Check the BQ state if no error before read
    // check BQ sanity only once per seconds
    if (HAL_GetTick() - m_timeLastSanity > m_delayBwtSanityBqCheck) {
        m_timeLastSanity = HAL_GetTick();
        ret = sanityBqCheck();
    }

    // Get the voltage value from BQ
    // todo : possibly manage a flag in case of BQ error (return value of sanityBqCheck) and in that case never call updateVoltage
    if (ret == 0) {
        ret = updateVoltage();
    }

    return ret;
}


//-------------------------------------------//
/// @fn get( )
/// @param information : from enum in the sub-class (cBq769x0) contain requested information
/// @brief get a specific value read by AFE
//-------------------------------------------//
unsigned long cBq769x0::get(tGetData information) {

    unsigned long ret(0);

    switch (information) {
        case eVoltageBattery:
            ret = m_batteryVoltage;
            break;
        case eVoltageLowest:
            ret = m_cellVoltages[m_indexCellMinVoltage];
            break;
        case eVoltageHightest:
            ret = m_cellVoltages[m_indexCellMaxVoltage];
            break;
        default:
            break;
    }

    return ret;
}


//-------------------------------------------//
/// @fn getCellVoltage( )
/// @param channel : requested channel
/// @brief get cell voltage value
//-------------------------------------------//
unsigned long cBq769x0::getCellVoltage(int channel) {

    unsigned long ret(0);

    if (channel > m_numberOfCells) {
        printf("cBq769x0::get : WARNING : requested a channel which is not connected");
    }else {
        ret = m_cellVoltages[channel];
    }

    return ret;
}


//-------------------------------------------//
/// @fn balance( )
/// @brief balance, so short circuit a cell
/// @param request : which cell wants to balance
//-------------------------------------------//
void cBq769x0::balance( tBalance request ) {

    switch (request) {
        case eLowest:
            balanceACell(m_indexCellMinVoltage);
            break;
        case eHightest:
            balanceACell(m_indexCellMaxVoltage);
            break;
        default:
            balanceACell(request);
            break;
    }
}


//-------------------------------------------//
/// @fn balanceACell( )
/// @brief balance a specified cell, so short circuit this cell
/// @param cellIndex : the index of the cell you want to balance
//-------------------------------------------//
void cBq769x0::balanceACell( int cellIndex ) {

    uint8_t balanceCell(0);
    int reg(CELLBAL1);

    // adjust parameters
    if (cellIndex >= 5) {
        if (cellIndex >= 10) {
            cellIndex -= 10;
            reg = CELLBAL3;
        }else {
            cellIndex -= 5;
            reg = CELLBAL2;
        }
    }

    // place a 1 in the bit of corresponding cell wants to balance (see datasheet Table 7-4 to 6 for more information)
    balanceCell = 0x1 << cellIndex;

    // sent data to the BQ
    write( reg, balanceCell);
}


//-------------------------------------------//
/// @fn updateVoltage( )
/// @brief update the voltage value to get data from BQ
//-------------------------------------------//
int cBq769x0::updateVoltage() {
    int ret(0);
    uint8_t connectedCells(0);

    // Read all the cell voltage registers
    uint8_t voltage[MAX_NUMBER_OF_CELLS];
    ret = read(VC1_HI_BYTE, m_numberOfCells*2, voltage);  // m_numberOfCells*2 because there is cell MSB and LSB

    if (ret == 0) {

        int voltIndex(0);
        for (int i = 0; i < m_numberOfCells; i++) {

            // Assembled the data
            unsigned int iTemp = (unsigned int)(voltage[voltIndex] << 8) + voltage[voltIndex+1];
            // Rescale data
            unsigned long cellMVoltValue = ((unsigned long)iTemp * m_adcGain) / 1000;
            cellMVoltValue += m_adcOffset;
            // Save the data
            m_cellVoltages[i] = cellMVoltValue;
            voltIndex += 2;

            // Consider if the cell value is upper than 500mV, there is a cell connected to this measure point
            if (cellMVoltValue > 500) {
                connectedCells++;
                if (cellMVoltValue < m_cellVoltages[m_indexCellMinVoltage]) {
                    m_indexCellMinVoltage = i;
                }
            }

            if (cellMVoltValue > m_cellVoltages[m_indexCellMaxVoltage]) {
                m_indexCellMaxVoltage = i;
            }
        }

        // read battery pack voltage
        uint8_t bat[2] = {0,0};
        ret = read(BAT_HI_BYTE, 1, bat);
        ret &= read(BAT_LO_BYTE, 1, bat+1);

        if (ret == 0) {
            unsigned int adcVal = (bat[0] << 8) + bat[1];
            m_batteryVoltage = (unsigned long) (4.0 * m_adcGain * adcVal / 1000.0 + connectedCells * m_adcOffset);  // todo : check if we never miss information in this cast
#ifdef BQ769X0_DEBUG
            printf("cBq769x0::update : DEBUG : Pack Voltage %ld\n", m_batteryVoltage);
            printf("Cell Voltage :");
            for (int i = 0; i < m_numberOfCells; i++) {
                printf(" %lu mV", m_cellVoltages[1]);
            }
            printf("\n");
#endif
        }
    }

    return ret;
}


//-------------------------------------------//
/// @fn findBqConfig( )
/// @brief Check the BQ chip type and crc feature enabled
///        We set an address and CRC state and check if we are able to write CC_CFG register
///        To verify if this ok, we try to read the CC_CFG register if not we try other settings
/// @retval return 0 if all ok, -1 if error
//-------------------------------------------//
int cBq769x0::findBqConfig( ) {

	int ret(-1);
    bool crcEnable(false);
	// Create a local CRC object
    cCrcSoft crc;

    if (isBqOnThisConfig(0x08, &crc)) {          // on I2C address 0x08 with crc enable
        m_i2cAddress = 0x08;
        crcEnable = true;
        ret = 0;
    }else if (isBqOnThisConfig(0x18, &crc)) {     // on I2C address 0x18 with crc enable
        m_i2cAddress = 0x18;
        crcEnable = true;
        ret = 0;
    }else if (isBqOnThisConfig(0x08, NULL)) {    // on I2C address 0x08 with crc disable
        m_i2cAddress = 0x08;
        ret = 0;
    }else if (isBqOnThisConfig(0x18, NULL)) {    // on I2C address 0x18 with crc disable
        m_i2cAddress = 0x18;
        ret = 0;
    }else {                                     // Here BQ chip is not responding ...
        // Reset the I2C settings
        m_i2cAddress = 0x0;
    }

    if (crcEnable && (m_crc == NULL)) {
        m_crc = new cCrcSoft();
    }

	return ret;
}


//-------------------------------------------//
/// @fn isBqOnThisConfig( )
/// @brief verify if the BQ respond with settings
/// @param i2cAddress: the test I2C address where we think the BQ should be set
/// @param crc: a pointer to a crc object to calculate the CRC
/// @retval true if the BQ is working with those settings, even false
//-------------------------------------------//
bool cBq769x0::isBqOnThisConfig(int i2cAddress, cCrc* crc) {
    
    bool ret(false);

    m_i2cAddress = i2cAddress;

    // Try to write the CC_CFG register
    write(CC_CFG, m_constValCC_CFG, crc);

    // Try to read back this register to know if there is a BQ connect with this settings
    uint8_t readVal(0);
    int error = read(CC_CFG, 1, &readVal);

 	if (readVal == m_constValCC_CFG && error == 0) {
#ifdef BQ769X0_DEBUG
        if (crc != NULL)
	        printf("cBq769x0::init : DEBUG : BQ I2C Address is 0x%02X\tand CRC is Enable\n", m_i2cAddress);
        else
        	printf("cBq769x0::init : DEBUG : BQ I2C Address is 0x%02X\tand CRC is Disable\n", m_i2cAddress);
#endif
		ret = true;
	}

    return ret;
}


//-------------------------------------------//
/// @fn clearBqError( )
/// @brief Method to check if BQ is in error, if yes it manage to fix it
/// @retval return 0 if all ok, -1 if error needs to be reset by clearBqError or -2 if too important error
//-------------------------------------------//
int cBq769x0::sanityBqCheck() {
	int ret(0);
	regSYS_STAT_t sys_stat;

	ret = read(SYS_STAT, 1, &sys_stat.regByte);  // Warning check if regByte work correctly

	// Correctly received the SYS_SAT register
	if (ret == 0) {
		if (sys_stat.regByte & STAT_FLAGS) {
#ifdef BQ769X0_DEBUG
			printf("cBq769x0::sanityBqCheck : WARNING : BQ have an error in SYS_STAT register = 0X%04x", sys_stat.regByte);
#endif
			// Try to fine a way to solve BQ issue
			ret = clearBqError(sys_stat);
		}

	}else { // Even try to reset all SYS_SAT register
#ifdef BQ769X0_DEBUG
		printf("cBq769x0::sanityBqCheck : WARNING : Impossible to read SYS_STAT register, but try to reset all BQ error");
#endif
		//Bits in SYS_STAT may be cleared by writing a "1" to the corresponding bit
		ret = write(SYS_STAT, 0b00111111);
		HAL_Delay(100);
		ret = read(SYS_STAT, 1, &sys_stat.regByte);
		if (ret != 0) {
			printf("cBq769x0::sanityBqCheck : ERROR : Fault error of the BQ, impossible to restart it");
			ret = -2;
		}
	}

	return ret;
}


//-------------------------------------------//
/// @fn clearBqError( )
/// @brief Method to manage and clear all BQ error
/// @param sys_stat: the system status flags
/// @retval return 0 if all ok, -1 if error
//-------------------------------------------//
int cBq769x0::clearBqError(regSYS_STAT_t sys_stat) {
	int ret(0);

	// Internal chip fault indicator
	if (sys_stat.bits.DEVICE_XREADY) {
		printf("cBq769x0::clearBqError : WARNING : Internal Fault ... clear it\n");
		// The BQ DataSheet request to wait few second before reset the fault
		HAL_Delay(2500); // wait 2,5s before reset
		ret = write(SYS_STAT, STAT_DEVICE_XREADY);
	}

	// External pull-up on the ALERT pin indicator
	if (sys_stat.bits.OVRD_ALERT) {
		printf("cBq769x0::clearBqError : WARNING : External pull-up on the ALERT pin indicator\n");
		ret = write(SYS_STAT, STAT_OVRD_ALERT);
	}

	// Unde rvoltage fault event indicator
	if (sys_stat.bits.UV) {
		// Check which cell is under voltage
		update();
		if (m_cellVoltages[m_indexCellMinVoltage] > m_minCellValue) {
			ret = write(SYS_STAT, STAT_UV);
			printf("cBq769x0::clearBqError : WARNING : UnderVoltage Cell %lu\n", m_cellVoltages[m_indexCellMinVoltage]);
			// todo: inform the Master over Communication
		}
	}
	// Over voltage fault event indicator
	if (sys_stat.regByte & STAT_OV)	{
		// Check which cell is over voltage
		update();
		if (m_cellVoltages[m_indexCellMaxVoltage] < m_maxCellValue)	{
			ret = write(SYS_STAT, STAT_OV);
			printf("cBq769x0::clearBqError : WARNING : OverVoltage Cell %lu\n", m_cellVoltages[m_indexCellMaxVoltage]);
			// todo: inform the Master over Communication
		}
	}

	return ret;
}


//-------------------------------------------//
/// @fn send( )
/// @brief send data to AFE only one byte can be wrote
/// @detail use the crc object to know if we use the crc: if NULL CRC checking is disable
/// @param regAddress: destination register address
/// @param value: the value to send to the BQ
/// @param crc: a reference to a crc object
/// @retval return 0 if all ok, -1 if error
//-------------------------------------------//
int cBq769x0::write(int regAddress, uint8_t value, cCrc* crc) {

    int ret(-1);

	if (crc != NULL) {
	    uint8_t buf[4] = {0,0,0,0};

        buf[0] = m_i2cAddress << 1;  // Will not be send, only for CRC computation
        buf[1] = (uint8_t) regAddress;
	    buf[2] = value;
        buf[3] = crc->run(buf, 3);

		ret = m_i2c->transmit(buf + 1, 3);

	}else {
        uint8_t buf[2] = {0,0};

        buf[0] = (uint8_t) regAddress;
	    buf[1] = value;
		ret = m_i2c->transmit(buf, 2);
	}

    return ret;
}


//-------------------------------------------//
/// @fn read( )
/// @brief receive data from AFE
/// @param regAddress: the register address which we want to know the value
/// @param length: number of byte wants to read (in byte)
/// @param readBuffer: a pointer where the read value will be stored (must have a size = to variable length)
/// @retval return 0 if all ok, -1 if error
//-------------------------------------------//
int cBq769x0::read(int regAddress, uint16_t length, uint8_t *readBuffer) {
	int ret(0);
    uint8_t crcVal(0);

    // Clear the return buffer
    for (int i = 0; i < length; i++) {
        readBuffer[i] = 0;
    }

    // Send the register which wants to read
	uint8_t targetRegister = (char) regAddress;
	ret = m_i2c->transmit(&targetRegister, 1);

    if (ret == 0) {
        // Case with CRC bytes
        if (m_crc != NULL) {
            uint8_t *readData = (uint8_t *)malloc(2 * length); // Temp buffer where the brute data read from I2C is loaded
            ret = m_i2c->receive(readData, length * 2); // length * 2 for all the CRC val
            // Data mapping:
            // | data1 | CRC data1 | data2 | CRC data2 | ...

            if (ret == 0) {
                uint8_t CRCInput[2];
                // CRC is calculated over the slave address (including R/W bit) and data.
                CRCInput[0] = (m_i2cAddress << 1) + 1;
	            CRCInput[1] = *readData;
                crcVal = m_crc->run(CRCInput, 2);

                readData++;
                // | data1 | CRC data1 | data2 | CRC data2 |
                //               #
                if (crcVal == *readData) {

                    // Save the data in output buffer
                    *readBuffer = *(readData - 1);

                    for(int i = 1; i < length && ret == 0; i++) {

                        readData++;
                        // | data1 | CRC data1 | data2 | CRC data2 |
                        //                        #
                        crcVal = m_crc->run(readData, 1);

                        readData++;
                        // | data1 | CRC data1 | data2 | CRC data2 |
                        //                                   #
                        if (crcVal != *readData) {
                            printf("cBq769x0::read : ERROR : Wrong CRC on data 0X%04x at index %d\n", *(readData - 1), i);
                            ret = -1;
                        }else {
                            // Save the data in output buffer
                            *readBuffer = *(readData - 1);
                        }
                    }
                    
                }else {
                    printf("cBq769x0::read : ERROR : Wrong CRC\n");
                    ret = -1;
                }
            }
            free(readData);
            readData = NULL;
        // Simple Case
        }else {
            ret = m_i2c->receive(readBuffer, length);
        }

    }else {
        printf("cBq769x0::read : ERROR : impossible to send targetRegister to BQ, error code = %d\n", ret);
    }

	return ret;
}


