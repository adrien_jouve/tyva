/*!
 ------------------------------------------------------------------------------
                   TYVA ENERGIE Technical Software Department
  ------------------------------------------------------------------------------
  Copyright (c) 2013-2015, TYVA ENERGIE.
  Address.
  All rights reserved.
  This source program is the property of TYVA ENERGIE Company mentioned above
  and may not be copied in any form or by any means, whether in part or in whole,
  except under license expressly granted by such TYVA ENERGIE company.
  All copies of this program, whether in part or in whole, and
  whether modified or not, must display this and all other
  embedded copyright and ownership notices in full.
  ------------------------------------------------------------------------------
 
  Project : BMS
  Module  : measure

  ------------------------------------------------------------------------------
  */

/**
******************************************************************************
* @file      cAfe.hpp
* @author    Adrien Jouve
* @date      January 15, 2019
* @brief     interface to access to an AFE peripheral
* @details   Should not have direct HAL access
******************************************************************************
*/
#ifndef _cAfe_
#define _cAfe_

#include <stdio.h>
#include "Drivers/cDriver.hpp"
#ifdef UNITTEST
#include "Stubs/stm32_stubs.h"
#endif

class cAfe: public cDriver {
    //----------------------------- Public Members ---------------------------//
    public:
        cAfe(){};
        ~cAfe(){};

        // Possible request on the Get
        enum tGetData {
            eVoltageBattery = 0,
            eVoltageLowest  = 1,
            eVoltageHightest= 2,
        };

        // Balance possibility
        enum tBalance {
            eLowest     = 0,
            eHightest   = 1,
        };

        //-------------------------------------------//
        /// @fn init( )
        /// @brief to initialized the AFE peripheral
        //-------------------------------------------//
        virtual int init( void ) = 0;

        //-------------------------------------------//
        /// @fn update( )
        /// @brief access to the AFE to get all values
        //-------------------------------------------//
        virtual int update( void ) = 0;

        //-------------------------------------------//
        /// @fn get( )
        /// @param information : from enum in the sub-class (cBq769x0) contain requested information
        /// @brief get a specific value read by AFE
        //-------------------------------------------//
        virtual unsigned long get( tGetData information ) = 0;

        //-------------------------------------------//
        /// @fn getCellVoltage( )
        /// @param channel : requested channel
        /// @brief get cell voltage value
        //-------------------------------------------//
        virtual unsigned long getCellVoltage( int channel ) = 0;

        //-------------------------------------------//
        /// @fn getNumberOfCells( )
        /// @brief return the number of cells
        //-------------------------------------------//
        virtual uint32_t getNumberOfCells( void ) = 0;

        //-------------------------------------------//
        /// @fn balance( )
        /// @brief balance, so short circuit a cell
        //-------------------------------------------//
        virtual void balance( tBalance request ) = 0;

};
#endif //_cAfe_
