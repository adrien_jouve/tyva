/*!
 ------------------------------------------------------------------------------
                   TYVA ENERGIE Technical Software Department
  ------------------------------------------------------------------------------
  Copyright (c) 2013-2015, TYVA ENERGIE.
  Address.
  All rights reserved.
  This source program is the property of TYVA ENERGIE Company mentioned above
  and may not be copied in any form or by any means, whether in part or in whole,
  except under license expressly granted by such TYVA ENERGIE company.
  All copies of this program, whether in part or in whole, and
  whether modified or not, must display this and all other
  embedded copyright and ownership notices in full.
  ------------------------------------------------------------------------------
 
  Project : BMS

  ------------------------------------------------------------------------------
  */

/**
******************************************************************************
* @file      cBq769x0.hpp
* @author    Adrien Jouve
* @date      January 15, 2019
* @brief     interface to access to a sensor
* @details   
******************************************************************************
*/

#ifndef _cBq769x0_
#define _cBq769x0_

#include "cAfe.hpp"
#include "bq769x0Register.h"
#include "tools/cCrcSoft.hpp"
#include "Drivers/I2C/cI2c.hpp"

#ifdef UNITTEST
#include "Stubs/stm32_stubs.h"
#else
#include "main.h"

#include "stm32l431xx.h"
#include "stm32l4xx_hal.h"
#endif


#define BQ769X0_DEBUG

class cBq769x0: public cAfe
{
    public:

        // All possible BQ type
        enum tTypes{
            eInternalADC    = 0,
            eBq76920        = 1,
            eBq76930        = 2,
            eBq76940        = 3,
        };

        cBq769x0(GPIO_TypeDef *BqPort, int BqPin, tTypes type, int minCellVal, int maxCellVal);
        ~cBq769x0();

        //-------------------------------------------//
        /// @fn init( )
        /// @brief to initialized the AFE peripheral
        /// @retval return 0 if all ok, -1 if error
        //-------------------------------------------//
        virtual int init( void );

        //-------------------------------------------//
        /// @fn update( )
        /// @brief access to the AFE to get all values
        /// @retval return 0 if all ok, -1 if error
        //-------------------------------------------//
        virtual int update( void );

        //-------------------------------------------//
        /// @fn get( )
        /// @param information : from enum in the sub-class (cBq769x0) contain requested information
        /// @brief get a specific value read by AFE
        //-------------------------------------------//
        virtual unsigned long get(tGetData information);

        //-------------------------------------------//
        /// @fn getCellVoltage( )
        /// @param channel : requested channel
        /// @brief get cell voltage value
        //-------------------------------------------//
        virtual unsigned long getCellVoltage( int channel );

        //-------------------------------------------//
        /// @fn getNumberOfCells( )
        /// @brief return the number of cells
        //-------------------------------------------//
        virtual uint32_t getNumberOfCells( void ) { return m_numberOfCells; };

        //-------------------------------------------//
        /// @fn balance( )
        /// @brief balance, so short circuit a cell
        //-------------------------------------------//
        virtual void balance( tBalance request );


    protected:

        //-------------------------------------------//
        /// @fn wakeupBqDisable( )
        /// @brief to Disable the wakeup the BQ peripheral
        //-------------------------------------------//
        void wakeupBqDisable(void);

        //-------------------------------------------//
        /// @fn wakeupBqEnable( )
        /// @brief to Enable the wakeup the BQ peripheral
        //-------------------------------------------//
        void wakeupBqEnable(void);

        //-------------------------------------------//
        /// @fn updateVoltage( )
        /// @brief update the voltage value to get data from BQ
        //-------------------------------------------//
        int updateVoltage();

        //-------------------------------------------//
        /// @fn findBqConfig( )
        /// @brief Check the BQ chip type and crc feature enabled
        ///        We set an address and CRC state and check if we are able to write CC_CFG register
        ///        To verify if this ok, we try to read the CC_CFG register if not we try other settings
        /// @retval return 0 if all ok, -1 if error
        //-------------------------------------------//
        int findBqConfig(void);

        //-------------------------------------------//
        /// @fn isBqOnThisConfig( )
        /// @brief verify if the BQ respond with settings
        /// @param i2cAddress: the test I2C address where we think the BQ should be set
        /// @param crc: a pointer to a crc object to calculate the CRC
        /// @retval true if the BQ is working with those settings, even false
        //-------------------------------------------//
        bool isBqOnThisConfig(int i2cAddress, cCrc* crc);

        //-------------------------------------------//
        /// @fn clearBqError( )
        /// @brief Method to manage and clear all BQ error
        /// @retval return 0 if all ok, -1 if error needs to be reset by clearBqError or -2 if too important error
        //-------------------------------------------//
        int sanityBqCheck(void);

        //-------------------------------------------//
        /// @fn clearBqError( )
        /// @brief Method to manage and clear all BQ error
        /// @param sys_stat: the system status flags
        /// @retval return 0 if all ok, -1 if error
        //-------------------------------------------//
        int clearBqError(regSYS_STAT_t sys_stat);

        //-------------------------------------------//
        /// @fn write( )
        /// @brief send data to AFE
        /// @param regAddress: destination register address
        /// @param value: the value to send to the BQ
        /// @param crc: a reference to a crc object
        /// @retval return 0 if all ok, -1 if error
        //-------------------------------------------//
        int write(int regAddress, uint8_t value, cCrc* crc);

        //-------------------------------------------//
        /// @fn send( )
        /// @brief send data to AFE
        /// @param regAddress: destination register address
        /// @param value: the value to send to the BQ
        /// @retval return 0 if all ok, -1 if error
        //-------------------------------------------//
        int write(int regAddress, uint8_t value){ return write(regAddress, value, m_crc); };

        //-------------------------------------------//
        /// @fn read( )
        /// @brief receive data from AFE
        /// @param regAddress: the register address which we want to know the value
        /// @param length: number of byte wants to read (in byte)
        /// @param readBuffer: a pointer where the read value will be stored (must have a size = to variable length)
        /// @retval return 0 if all ok, -1 if error
        //-------------------------------------------//
        int read(int regAddress, uint16_t length, uint8_t *readBuffer);

        //-------------------------------------------//
        /// @fn balanceACell( )
        /// @brief balance a specified cell, so short circuit this cell
        /// @param cellIndex : the index of the cell you want to balance
        //-------------------------------------------//
        void balanceACell( int cellIndex );

        // The GPIO port where the BQ is connected
        GPIO_TypeDef* m_bqPort;

        // The GPIO pin number where the BQ is connected
        int m_bqPinNumber;

        // The current type of BQ connected
        tTypes m_bqType;

        // Number of cells on this device
        int m_numberOfCells;

        // Cell voltage value in mV
        unsigned long m_cellVoltages[MAX_NUMBER_OF_CELLS];

        // Battery voltage value in mV
        unsigned long m_batteryVoltage;

        // Index of Min and Max cell
        int m_indexCellMinVoltage;
        int m_indexCellMaxVoltage;

        // The min/max accepted voltage value for a cell
        unsigned long m_minCellValue;
        unsigned long m_maxCellValue;

        // BQ I2C bus address, find in the datasheet (all TI BQ have a specific I2C address set in production)
        int m_i2cAddress;

        // The constant value which shall be set in CC_CFG register
        static uint8_t const m_constValCC_CFG = 0x19;

        // A pointer to the crc calculator tool
        cCrc* m_crc;

        // A pointer to I2C object to communicate with AFE
        cI2c* m_i2c;

        // The AFE adc Gain in uV/LSB
        uint8_t m_adcGain;

        // The offset of the AFE in mV
        uint8_t m_adcOffset;

        // The time in ms of the last sanity check on BQ
        uint32_t m_timeLastSanity;

        // Delay between each sanity check of BQ (in ms)
        static uint32_t const m_delayBwtSanityBqCheck = 1000;

};
#endif //_cBq769x0_
