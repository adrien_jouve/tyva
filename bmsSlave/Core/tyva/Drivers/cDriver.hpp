/*!
 ------------------------------------------------------------------------------
                   TYVA ENERGIE Technical Software Department
  ------------------------------------------------------------------------------
  Copyright (c) 2013-2015, TYVA ENERGIE.
  Address.
  All rights reserved.
  This source program is the property of TYVA ENERGIE Company mentioned above
  and may not be copied in any form or by any means, whether in part or in whole,
  except under license expressly granted by such TYVA ENERGIE company.
  All copies of this program, whether in part or in whole, and
  whether modified or not, must display this and all other
  embedded copyright and ownership notices in full.
  ------------------------------------------------------------------------------
 
  Project : BMS
  Module  : measure

  ------------------------------------------------------------------------------
  */

/**
******************************************************************************
* @file      cDriver.hpp
* @author    Adrien Jouve
* @date      January 15, 2019
* @brief     interface to access to a sensor
* @details   
******************************************************************************
*/
#ifndef _cDriver_
#define _cDriver_

#include <stdio.h>

#ifdef UNITTEST
#include "Stubs/stm32_stubs.h"
#endif

class cDriver
{
    public:
        cDriver(){};
        ~cDriver(){};

        //-------------------------------------------//
        /// @fn init( )
        /// @brief to initialized the driver
        //-------------------------------------------//
        virtual int init( void ) = 0;
};
#endif //_cDriver_
