/*!
 ------------------------------------------------------------------------------
                   TYVA ENERGIE Technical Software Department
  ------------------------------------------------------------------------------
  Copyright (c) 2013-2015, TYVA ENERGIE.
  Address.
  All rights reserved.
  This source program is the property of TYVA ENERGIE Company mentioned above
  and may not be copied in any form or by any means, whether in part or in whole,
  except under license expressly granted by such TYVA ENERGIE company.
  All copies of this program, whether in part or in whole, and
  whether modified or not, must display this and all other
  embedded copyright and ownership notices in full.
  ------------------------------------------------------------------------------
 
  Project : BMS

  ------------------------------------------------------------------------------
  */

/**
******************************************************************************
* @file      cMasterComm.hpp
* @author    ajouve
* @date      19 f�vr. 2019
* @brief     master communication interface
* @details   
******************************************************************************
*/


#ifndef TYVA_DRIVERS_CMASTERCOMM_HPP_
#define TYVA_DRIVERS_CMASTERCOMM_HPP_

#include "Drivers/cDriver.hpp"


class cMasterComm : public cDriver {
	//----------------------------- Public Members ---------------------------//
    public:
		cMasterComm(){};
		~cMasterComm(){};

        //-------------------------------------------//
        /// @fn init( )
        /// @brief to initialized CAN peripheral
        //-------------------------------------------//
        virtual int init( void ) = 0;

        //-------------------------------------------//
        /// @fn isDataAvailable( )
        /// @brief return true a new data is available even false
        //-------------------------------------------//
        virtual bool isDataAvailable() = 0;

        //-------------------------------------------//
        /// @fn getSize( )
        /// @brief return the size of available data
        //-------------------------------------------//
        virtual uint32_t getSize() = 0;

        //-------------------------------------------//
        /// @fn get( )
        /// @brief return available data on comm link
        //-------------------------------------------//
        virtual uint8_t* get() = 0;

        //-------------------------------------------//
        /// @fn receive( )
        /// @brief receive data from the CAN
        //-------------------------------------------//
        virtual void receive() = 0;

        //-------------------------------------------//
        /// @fn transmit( )
        /// @brief send data to the master over CAN
        /// @param pData: pointer to data we want to send
        /// @param size: the size of data want to send
        /// @retval return 0 if ok, -1 if error
        //-------------------------------------------//
        virtual int transmit(uint32_t *pData, uint32_t size) = 0;

        //-------------------------------------------//
        /// @fn transmit( )
        /// @brief send data to the master over CAN
        /// @param pData: pointer to data we want to send
        /// @param size: the size of data want to send
        /// @retval return 0 if ok, -1 if error
        //-------------------------------------------//
        virtual int transmit(uint8_t *pData, uint32_t size) = 0;

	//---------------------------- Protected Members -------------------------//
    protected:

};

#endif // TYVA_DRIVERS_CMASTERCOMM_HPP_
