/*!
 ------------------------------------------------------------------------------
                   TYVA ENERGIE Technical Software Department
  ------------------------------------------------------------------------------
  Copyright (c) 2013-2015, TYVA ENERGIE.
  Address.
  All rights reserved.
  This source program is the property of TYVA ENERGIE Company mentioned above
  and may not be copied in any form or by any means, whether in part or in whole,
  except under license expressly granted by such TYVA ENERGIE company.
  All copies of this program, whether in part or in whole, and
  whether modified or not, must display this and all other
  embedded copyright and ownership notices in full.
  ------------------------------------------------------------------------------
 
  Project : BMS Slave
  
  ------------------------------------------------------------------------------
  */

/**
******************************************************************************
* @file      cRtc.hpp
* @author    Adrien Jouve
* @date      January 31, 2019
* @brief     interface to access to the RTC peripheral
* @details   
******************************************************************************
*/
#ifndef _cRtc_
#define _cRtc_

#include "cDriver.hpp"

class cRtc: public cDriver
{
    public:
        //-------------------------------------------//
        /// @fn cRtc( )
        /// @brief cRtc constructor
        //-------------------------------------------//
        cRtc(){
            printf("build cRtc object cRtc\n");
        };
        ~cRtc(){};

        //-------------------------------------------//
        /// @fn init( )
        /// @brief to initialized RTC peripheral
        //-------------------------------------------//
        virtual int init( void ){ return 0; };

        //-------------------------------------------//
        /// @fn get( )
        /// @brief get the rtc value
        //-------------------------------------------//
        virtual int get( void ){ return 0; };

};
#endif //_cRtc_
