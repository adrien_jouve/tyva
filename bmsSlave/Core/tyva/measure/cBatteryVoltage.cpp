/*!
 ------------------------------------------------------------------------------
                   TYVA ENERGIE Technical Software Department
  ------------------------------------------------------------------------------
  Copyright (c) 2013-2015, TYVA ENERGIE.
  Address.
  All rights reserved.
  This source program is the property of TYVA ENERGIE Company mentioned above
  and may not be copied in any form or by any means, whether in part or in whole,
  except under license expressly granted by such TYVA ENERGIE company.
  All copies of this program, whether in part or in whole, and
  whether modified or not, must display this and all other
  embedded copyright and ownership notices in full.
  ------------------------------------------------------------------------------
 
  Project : BMS

  ------------------------------------------------------------------------------
  */

/**
******************************************************************************
* @file      cBatteryVoltage.cpp
* @author    ajouve
* @date      19 f�vr. 2019
* @brief     manage battery informations
* @details   
******************************************************************************
*/


#include "cBatteryVoltage.hpp"


//-------------------------------------------//
/// @fn cBatteryVoltage( )
/// @brief cBatteryVoltage constructor
//-------------------------------------------//
cBatteryVoltage::cBatteryVoltage() {
}

//-------------------------------------------//
/// @fn ~cBatteryVoltage( )
/// @brief cBatteryVoltage destructor
//-------------------------------------------//
cBatteryVoltage::~cBatteryVoltage(){
}


//-------------------------------------------//
/// @fn init( )
/// @brief to initialized object
/// @retval return 0 if all ok, -1 if error
//-------------------------------------------//
int cBatteryVoltage::init( cAfe* afe ) {
    int ret(0);

    m_afe = afe;

    return ret;
}


//-------------------------------------------//
/// @fn get( )
/// @brief obtain the measured value the battery Voltage
/// @param retArray: a pointer to an array to return voltage values
/// @retval int: return 0 if all ok, -1 if error
//-------------------------------------------//
int cBatteryVoltage::get( uint32_t* retArray ) {
    int ret(0);

    *retArray = m_afe->get(cAfe::eVoltageBattery);

    return ret;
}



