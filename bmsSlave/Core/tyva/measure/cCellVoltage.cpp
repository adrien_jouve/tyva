/*!
 ------------------------------------------------------------------------------
                   TYVA ENERGIE Technical Software Department
  ------------------------------------------------------------------------------
  Copyright (c) 2013-2015, TYVA ENERGIE.
  Address.
  All rights reserved.
  This source program is the property of TYVA ENERGIE Company mentioned above
  and may not be copied in any form or by any means, whether in part or in whole,
  except under license expressly granted by such TYVA ENERGIE company.
  All copies of this program, whether in part or in whole, and
  whether modified or not, must display this and all other
  embedded copyright and ownership notices in full.
  ------------------------------------------------------------------------------
 
  Project : BMS

  ------------------------------------------------------------------------------
  */

/**
******************************************************************************
* @file      cCellVoltage.cpp
* @author    ajouve
* @date      14 f�vr. 2019
* @brief     
* @details   
******************************************************************************
*/


#include "cCellVoltage.hpp"


//-------------------------------------------//
/// @fn cCellVoltage( )
/// @brief cCellVoltage constructor
//-------------------------------------------//
cCellVoltage::cCellVoltage( uint32_t numberOfCells):
    m_numberOfCells(numberOfCells)
{
}


//-------------------------------------------//
/// @fn ~cVoltage( )
/// @brief cVoltage destructor
//-------------------------------------------//
cCellVoltage::~cCellVoltage(){
}


//-------------------------------------------//
/// @fn init( )
/// @brief to initialized object
/// @retval return 0 if all ok, -1 if error
//-------------------------------------------//
int cCellVoltage::init( cAfe* afe ) {
    int ret(0);

    m_afe = afe;

    return ret;
}


//-------------------------------------------//
/// @fn get( )
/// @brief obtain the measured value for all channels
/// @param retArray: a pointer to an array to return voltage values
/// @retval int: return 0 if all ok, -1 if error
//-------------------------------------------//
int cCellVoltage::get(uint32_t* retArray) {
    int ret(0);

    for (uint32_t cellIndex = 0; cellIndex < m_numberOfCells; cellIndex++ ) {
        retArray[cellIndex] = m_afe->getCellVoltage(cellIndex);
    }

    return ret;
}




