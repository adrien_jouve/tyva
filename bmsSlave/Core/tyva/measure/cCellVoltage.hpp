/*!
 ------------------------------------------------------------------------------
                   TYVA ENERGIE Technical Software Department
  ------------------------------------------------------------------------------
  Copyright (c) 2013-2015, TYVA ENERGIE.
  Address.
  All rights reserved.
  This source program is the property of TYVA ENERGIE Company mentioned above
  and may not be copied in any form or by any means, whether in part or in whole,
  except under license expressly granted by such TYVA ENERGIE company.
  All copies of this program, whether in part or in whole, and
  whether modified or not, must display this and all other
  embedded copyright and ownership notices in full.
  ------------------------------------------------------------------------------
 
  Project : BMS

  ------------------------------------------------------------------------------
  */

/**
******************************************************************************
* @file      cCellVoltage.hpp
* @author    ajouve
* @date      14 f�vr. 2019
* @brief     to obtain all the voltage values of each cells
* @details   
******************************************************************************
*/


#ifndef TYVA_MEASURE_CCELLVOLTAGE_HPP_
#define TYVA_MEASURE_CCELLVOLTAGE_HPP_

#include "cGenericMeasureChannel.hpp"


class cCellVoltage : public cGenericMeasureChannel {
	//----------------------------- Public Members ---------------------------//
    public:
		cCellVoltage(uint32_t numberOfCells);
		~cCellVoltage();

        //-------------------------------------------//
        /// @fn init( )
        /// @brief to initialized object
        /// @retval return 0 if all ok, -1 if error
        //-------------------------------------------//
        virtual int init( cAfe* afe );

		//-------------------------------------------//
        /// @fn get( )
        /// @brief obtain the measured value for all channels
		/// @param retArray: a pointer to an array to return voltage values
		/// @retval int: return 0 if all ok, -1 if error
        //-------------------------------------------//
        virtual int get( uint32_t* retArray );


	//---------------------------- Protected Members -------------------------//
    protected:

        // Reference to the AFE object to request measure
		cAfe* m_afe;

        // Cells number on this slave
		uint32_t m_numberOfCells;

};

#endif // TYVA_MEASURE_CCELLVOLTAGE_HPP_
