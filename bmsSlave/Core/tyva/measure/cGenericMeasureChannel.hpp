/*!
 ------------------------------------------------------------------------------
                   TYVA ENERGIE Technical Software Department
  ------------------------------------------------------------------------------
  Copyright (c) 2013-2015, TYVA ENERGIE.
  Address.
  All rights reserved.
  This source program is the property of TYVA ENERGIE Company mentioned above
  and may not be copied in any form or by any means, whether in part or in whole,
  except under license expressly granted by such TYVA ENERGIE company.
  All copies of this program, whether in part or in whole, and
  whether modified or not, must display this and all other
  embedded copyright and ownership notices in full.
  ------------------------------------------------------------------------------
 
  Project : BMS
  Module  : measure

  ------------------------------------------------------------------------------
  */

/**
******************************************************************************
* @file      cGenericMeasureChannel.hpp
* @author    Adrien Jouve
* @date      January 15, 2019
* @brief     interface to access to a measure
* @details   
******************************************************************************
*/

#ifndef TYVA_MEASURE_CGENERICMEASURECHANNEL_HPP_
#define TYVA_MEASURE_CGENERICMEASURECHANNEL_HPP_

#include "Drivers/AFE/cAfe.hpp"


class cGenericMeasureChannel {
    //----------------------------- Public Members ---------------------------//
    public:
        cGenericMeasureChannel(){};
        ~cGenericMeasureChannel(){};

        //-------------------------------------------//
        /// @fn init( )
        /// @brief to initialized object
        /// @retval return 0 if all ok, -1 if error
        //-------------------------------------------//
        virtual int init( cAfe* afe ) = 0;

        //-------------------------------------------//
        /// @fn get( )
        /// @brief obtain the measured value
        /// @param retArray: a pointer to an array to return voltage values
        /// @retval int: return 0 if all ok, -1 if error
        //-------------------------------------------//
        virtual int get( uint32_t* retArray ) = 0;
};
#endif //TYVA_MEASURE_CGENERICMEASURECHANNEL_HPP_
