/*!
 ------------------------------------------------------------------------------
                   TYVA ENERGIE Technical Software Department
  ------------------------------------------------------------------------------
  Copyright (c) 2013-2015, TYVA ENERGIE.
  Address.
  All rights reserved.
  This source program is the property of TYVA ENERGIE Company mentioned above
  and may not be copied in any form or by any means, whether in part or in whole,
  except under license expressly granted by such TYVA ENERGIE company.
  All copies of this program, whether in part or in whole, and
  whether modified or not, must display this and all other
  embedded copyright and ownership notices in full.
  ------------------------------------------------------------------------------
 
  Project : BMS
  Module  : measure

  ------------------------------------------------------------------------------
  */

/**
******************************************************************************
* @file      cSyslog.hpp
* @author    Adrien Jouve
* @date      January 15, 2019
* @brief     Provide a mechanism to print log information
* @details   
******************************************************************************
*/
#ifndef _cSyslog_
#define _cSyslog_

#include <stdio.h>
#include <iostream>
#include <typeinfo>

using namespace std;

class cSyslog {
    public:
        //-------------------------------------------//
        /// @fn cSyslog( )
        /// @brief cSyslog constructor
        //-------------------------------------------//
        cSyslog(){};
        ~cSyslog(){};

        //-------------------------------------------//
        /// @fn error( )
        /// @brief log error information
        //-------------------------------------------//
        int error(const char *__format, ...){ return printf(__format); };

        //-------------------------------------------//
        /// @fn error( )
        /// @brief log error information
        //-------------------------------------------//
        int debug(const char *__format, string name, ...){
            string sformat(__format);
            sformat = "DEBUG: " + name + " : " + sformat;
            
            return printf(sformat.c_str());
        };
};

#endif //_cSyslog_
