/*!
 ------------------------------------------------------------------------------
                   TYVA ENERGIE Technical Software Department
  ------------------------------------------------------------------------------
  Copyright (c) 2013-2015, TYVA ENERGIE.
  Address.
  All rights reserved.
  This source program is the property of TYVA ENERGIE Company mentioned above
  and may not be copied in any form or by any means, whether in part or in whole,
  except under license expressly granted by such TYVA ENERGIE company.
  All copies of this program, whether in part or in whole, and
  whether modified or not, must display this and all other
  embedded copyright and ownership notices in full.
  ------------------------------------------------------------------------------
 
  Project : BMS

  ------------------------------------------------------------------------------
  */

/**
******************************************************************************
* @file      cCrcSoft.cpp
* @author    Adrien Jouve
* @date      February 5, 2019
* @brief     Object to calculate the CRC
* @details   
******************************************************************************
*/

#include "cCrcSoft.hpp"
#include <stdio.h>

//-------------------------------------------//
/// @fn run( )
/// @brief calculate the CRC (that the exact code of the BQ769x0 sample (Maximo))
/// @param inputVal: is a pointer to array with all input value on which the CRC will be calculated
/// @param len: the number of CRC to calculate
//-------------------------------------------//
uint8_t cCrcSoft::run(uint8_t* inputVal, uint8_t len) {
	uint8_t i;
	uint8_t crc(0);

    printf("cCrc::run : DEBUG : data before CRC calculation = 0x%04x\n", inputVal[0]);

	while(len--!=0)
	{
		for(i=0x80; i!=0; i/=2)
		{
			if((crc & 0x80) != 0)
			{
				crc *= 2;
				crc ^= m_crcKey;
			}
			else
				crc *= 2;

			if((*inputVal & i)!=0)
				crc ^= m_crcKey;
		}
		inputVal++;
	}
    
    printf("cCrc::run : DEBUG : CRC calculation = 0x%04x\n", crc);

	return crc;
}
