/*!
 ------------------------------------------------------------------------------
                   TYVA ENERGIE Technical Software Department
  ------------------------------------------------------------------------------
  Copyright (c) 2013-2015, TYVA ENERGIE.
  Address.
  All rights reserved.
  This source program is the property of TYVA ENERGIE Company mentioned above
  and may not be copied in any form or by any means, whether in part or in whole,
  except under license expressly granted by such TYVA ENERGIE company.
  All copies of this program, whether in part or in whole, and
  whether modified or not, must display this and all other
  embedded copyright and ownership notices in full.
  ------------------------------------------------------------------------------
 
  Project : BMS

  ------------------------------------------------------------------------------
  */

/**
******************************************************************************
* @file      cCrc.hpp
* @author    Adrien Jouve
* @date      February 5, 2019
* @brief     Object to calculate the CRC
* @details   
******************************************************************************
*/

#ifndef _cCrc_
#define _cCrc_

#ifdef UNITTEST
#include "Stubs/stm32_stubs.h"
#else
#include "stm32l431xx.h"
#endif

class cCrc
{
    public:
        cCrc(){};
        virtual ~cCrc(){};

        //-------------------------------------------//
        /// @fn run( )
        /// @brief calculate the CRC
        /// @param inputVal: is a pointer to array with all input value on which the CRC will be calculated
        /// @param len: the number of CRC to calculate
        //-------------------------------------------//
        virtual uint8_t run(uint8_t* inputVal, uint8_t len) = 0;

};


#endif //_cCrc_
