/*!
 ------------------------------------------------------------------------------
                   TYVA ENERGIE Technical Software Department
  ------------------------------------------------------------------------------
  Copyright (c) 2013-2015, TYVA ENERGIE.
  Address.
  All rights reserved.
  This source program is the property of TYVA ENERGIE Company mentioned above
  and may not be copied in any form or by any means, whether in part or in whole,
  except under license expressly granted by such TYVA ENERGIE company.
  All copies of this program, whether in part or in whole, and
  whether modified or not, must display this and all other
  embedded copyright and ownership notices in full.
  ------------------------------------------------------------------------------

  Project : BMS
  Module  : measure

  ------------------------------------------------------------------------------
  */

/**
******************************************************************************
* @file      cCan.cpp
* @author    Adrien Jouve
* @date      January 15, 2019
* @brief     interface to access to CAN communication layer
* @details
******************************************************************************
*/

#include "cProtocol.hpp"
#include "services/cBalance.hpp"

#ifdef UNITTEST
#include "Stubs/stm32_HAL_stubs.h"
#else
#include "main.h"
#endif

//-------------------------------------------//
/// @fn cProtocol( )
/// @brief cProtocol constructor
//-------------------------------------------//
cProtocol::cProtocol( 	cMasterComm& can,
                        cGenericMeasureChannel& voltMeasure,
                        cGenericMeasureChannel& batteryVoltageMeasure,
                        cGenericSensor& balance,
						unsigned long numberOfCells):
	m_master (can),
	m_voltageMeasure (voltMeasure),
	m_batteryVoltageMeasure(batteryVoltageMeasure),
	m_balance(balance),
	m_numberOfCells(numberOfCells)
{
}


//-------------------------------------------//
/// @fn run( )
/// @brief manage the data from the CAN
///      call the corresponding service to answer the CAN request
//-------------------------------------------//
int cProtocol::run() {

	int ret(0);

//	// Get received data from the CAN bus
//	uint32_t size = m_master.getSize();
	// todo : verify if the size of the buffer is already in bytes

	uint8_t* data;
	data = m_master.get();

//	uint8_t size = data[0] << 8 | data[1];

	switch (data[0]) {
		case eGetAllChannelVoltage:
			manageAllChannelVoltageRequest();
			break;

		case eGetBatteryVoltage:
		    manageBatteryVoltageRequest();
		    break;

		case eSetBalanceCell:
		    manageCellBalancingRequest(data + 1);
		    break;
		default:
			break;
	}


	return ret;
}


//-------------------------------------------//
/// @fn manageAllChannelVoltageRequest( )
/// @brief send all voltage value of each cells
///        to Master
///    |eGetAllChannelVoltage|
//-------------------------------------------//
void cProtocol::manageAllChannelVoltageRequest() {

	// Array contain the cells voltage
    uint32_t voltageArray[MAX_NUMBER_OF_CELLS + m_headerSize];

	// Manage the header of the buffer to transmit
	voltageArray[0] = eGetAllChannelVoltage;

	// Get the voltage measure
	m_voltageMeasure.get(voltageArray + m_headerSize);

	// Calculate the size of data wants to transmit
	uint32_t dataSize = m_numberOfCells + m_headerSize;

	// Transmit the voltage cells values
	m_master.transmit(voltageArray, dataSize);
}


//-------------------------------------------//
/// @fn manageBatteryVoltageRequest( )
/// @brief send battery voltage value to Master
///    |eGetBatteryVoltage|
//-------------------------------------------//
void cProtocol::manageBatteryVoltageRequest() {

    uint32_t batteryValue[1 + m_headerSize];

    // Manage the header of the buffer to transmit
    batteryValue[0] = eGetBatteryVoltage;

    // Get battery voltage measure
    m_batteryVoltageMeasure.get(batteryValue + m_headerSize);

    // Calculate the size of data wants to transmit
    uint32_t dataSize = 1 + m_headerSize;

    // Transmit the voltage cells values
    m_master.transmit(batteryValue, dataSize);
}

//-------------------------------------------//
/// @fn manageCellBalancingRequest( )
/// @brief balance the requested cell
///    |eSetBalanceCell|tCellBalancing|
//-------------------------------------------//
void cProtocol::manageCellBalancingRequest(uint8_t* data) {

    int cellToBalance(0);

    switch (*data) {
        case eBalanceCell1:
            cellToBalance = cBalance::eBalanceCell1;
            break;
        case eBalanceCell2:
            cellToBalance = cBalance::eBalanceCell2;
            break;
        case eBalanceCell3:
            cellToBalance = cBalance::eBalanceCell3;
            break;
        case eBalanceCell4:
            cellToBalance = cBalance::eBalanceCell4;
            break;
        case eBalanceCell5:
            cellToBalance = cBalance::eBalanceCell5;
            break;
        case eBalanceCell6:
            cellToBalance = cBalance::eBalanceCell6;
            break;
        case eBalanceCell7:
            cellToBalance = cBalance::eBalanceCell7;
            break;
        case eBalanceCell8:
            cellToBalance = cBalance::eBalanceCell8;
            break;
        case eBalanceCell9:
            cellToBalance = cBalance::eBalanceCell9;
            break;
        case eBalanceCell10:
            cellToBalance = cBalance::eBalanceCell10;
            break;
        case eBalanceCell11:
            cellToBalance = cBalance::eBalanceCell11;
            break;
        case eBalanceCell12:
            cellToBalance = cBalance::eBalanceCell12;
            break;
        case eBalanceCell13:
            cellToBalance = cBalance::eBalanceCell13;
            break;
        case eBalanceCell14:
            cellToBalance = cBalance::eBalanceCell14;
            break;
        case eBalanceCell15:
            cellToBalance = cBalance::eBalanceCell15;
            break;
        case eBalanceLowest:
            cellToBalance = cBalance::eBalanceLowest;
            break;
        case eBalanceHightest:
            cellToBalance = cBalance::eBalanceHightest;
            break;
        default:
            break;
    }

    // Call the balance with the master's request
    m_balance.run(cellToBalance);
}

