/*!
 ------------------------------------------------------------------------------
                   TYVA ENERGIE Technical Software Department
  ------------------------------------------------------------------------------
  Copyright (c) 2013-2015, TYVA ENERGIE.
  Address.
  All rights reserved.
  This source program is the property of TYVA ENERGIE Company mentioned above
  and may not be copied in any form or by any means, whether in part or in whole,
  except under license expressly granted by such TYVA ENERGIE company.
  All copies of this program, whether in part or in whole, and
  whether modified or not, must display this and all other
  embedded copyright and ownership notices in full.
  ------------------------------------------------------------------------------

  Project : BMS
  Module  : measure

  ------------------------------------------------------------------------------
  */

/**
******************************************************************************
* @file      cProtocol.hpp
* @author    Adrien Jouve
* @date      January 15, 2019
* @brief     protocol layer for the CAN communication interface
* @details
******************************************************************************
*/

#ifndef TYVA_PROTOCOL_CPROTOCOL_HPP_
#define TYVA_PROTOCOL_CPROTOCOL_HPP_

#include "Drivers/cMasterComm.hpp"
#include "measure/cGenericMeasureChannel.hpp"
#include "services/cGenericSensor.hpp"

class cProtocol
{
    public:
		//-------------------------------------------//
		/// @fn cProtocol( )
		/// @brief cProtocol constructor
		//-------------------------------------------//
		cProtocol( cMasterComm& can, cGenericMeasureChannel& voltMeasure, cGenericMeasureChannel& batteryVoltMeasure, cGenericSensor& balance, unsigned long numberOfCells );
		~cProtocol(){};

		//-------------------------------------------//
		/// @fn run( )
		/// @brief manage the data from the CAN
		//-------------------------------------------//
		int run();


    protected:
		//-------------------------------------------//
		/// @fn manageAllChannelVoltageRequest( )
		/// @brief manage the data from the CAN
		///      call the corresponding service to answer the CAN request
		//-------------------------------------------//
		void manageAllChannelVoltageRequest();


		//-------------------------------------------//
		/// @fn manageBatteryVoltageRequest( )
		/// @brief send battery voltage value to Master
		///    |eGetBatteryVoltage|
		//-------------------------------------------//
		void manageBatteryVoltageRequest();


		//-------------------------------------------//
		/// @fn manageCellBalancingRequest( )
		/// @brief balance the requested cell
		//-------------------------------------------//
		void manageCellBalancingRequest( uint8_t* data );


		// Reference to the CAN object
		cMasterComm& m_master;

		// Reference to the VoltageMeasure object
		cGenericMeasureChannel& m_voltageMeasure;

        // Reference to the BatteryVoltageMeasure object
        cGenericMeasureChannel& m_batteryVoltageMeasure;

		// Reference to object to manage balancing system
		cGenericSensor& m_balance;

		// Protocol request form master (the first value in received array)
		enum tRequestCan {
			eGetAllChannelVoltage	= 0x01,
			eGetBatteryVoltage		= 0x02,

			eSetBalanceCell			= 0x81,
		};

		// When receive eSetBalanceCell request, the second value can be one of following
		enum tCellBalancing {
		    eBalanceCell1   = 0x00,
            eBalanceCell2,
            eBalanceCell3,
            eBalanceCell4,
            eBalanceCell5,
            eBalanceCell6,
            eBalanceCell7,
            eBalanceCell8,
            eBalanceCell9,
            eBalanceCell10,
            eBalanceCell11,
            eBalanceCell12,
            eBalanceCell13,
            eBalanceCell14,
            eBalanceCell15,
            eBalanceLowest  = 0x20,
            eBalanceHightest,
		};

		// header size in byte to send data to the Master
		static uint8_t const m_headerSize = 1;	// |Frame ID|data|...

		// Cells number on this slave
		unsigned long m_numberOfCells;

};


#endif /* TYVA_PROTOCOL_CPROTOCOL_HPP_ */
