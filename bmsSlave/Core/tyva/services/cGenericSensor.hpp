/*!
 ------------------------------------------------------------------------------
                   TYVA ENERGIE Technical Software Department
  ------------------------------------------------------------------------------
  Copyright (c) 2013-2015, TYVA ENERGIE.
  Address.
  All rights reserved.
  This source program is the property of TYVA ENERGIE Company mentioned above
  and may not be copied in any form or by any means, whether in part or in whole,
  except under license expressly granted by such TYVA ENERGIE company.
  All copies of this program, whether in part or in whole, and
  whether modified or not, must display this and all other
  embedded copyright and ownership notices in full.
  ------------------------------------------------------------------------------
 
  Project : BMS

  ------------------------------------------------------------------------------
  */

/**
******************************************************************************
* @file      cGenericSensor.hpp
* @author    ajouve
* @date      15 f�vr. 2019
* @brief     
* @details   
******************************************************************************
*/


#ifndef TYVA_SERVICES_CGENERICSENSOR_HPP_
#define TYVA_SERVICES_CGENERICSENSOR_HPP_

#include "Drivers/AFE/cAfe.hpp"

class cGenericSensor {
	//----------------------------- Public Members ---------------------------//
    public:
		cGenericSensor(){};
		~cGenericSensor(){};

        //-------------------------------------------//
        /// @fn init( )
        /// @brief to initialized object
        /// @retval return 0 if all ok, -1 if error
        //-------------------------------------------//
        virtual int init( cAfe* afe ) = 0;

        //-------------------------------------------//
        /// @fn run( )
        /// @brief run something
        /// @param request : a parameter
        //-------------------------------------------//
        virtual void run( int request ) = 0;

};

#endif // TYVA_SERVICES_CGENERICSENSOR_HPP_
