/*!
 ------------------------------------------------------------------------------
                   TYVA ENERGIE Technical Software Department
  ------------------------------------------------------------------------------
  Copyright (c) 2013-2015, TYVA ENERGIE.
  Address.
  All rights reserved.
  This source program is the property of TYVA ENERGIE Company mentioned above
  and may not be copied in any form or by any means, whether in part or in whole,
  except under license expressly granted by such TYVA ENERGIE company.
  All copies of this program, whether in part or in whole, and
  whether modified or not, must display this and all other
  embedded copyright and ownership notices in full.
  ------------------------------------------------------------------------------
 
  Project : BMS

  ------------------------------------------------------------------------------
  */

/**
******************************************************************************
* @file      cBalance.hpp
* @author    ajouve
* @date      14 f�vr. 2019
* @brief     Cells balancing management
* @details   
******************************************************************************
*/


#ifndef TYVA_MEASURE_CBALANCE_HPP_
#define TYVA_MEASURE_CBALANCE_HPP_

#include "cGenericSensor.hpp"


class cBalance : public cGenericSensor {
	//----------------------------- Public Members ---------------------------//
    public:
		cBalance();
		~cBalance();

        //-------------------------------------------//
        /// @fn init( )
        /// @brief to initialized object
        /// @retval return 0 if all ok, -1 if error
        //-------------------------------------------//
        virtual int init( cAfe* afe );

        // enum with all the possible run value
		// Warning the cell 1 to 15 must be in that order because we use this index to balance
        enum tRun {
            eBalanceCell1   = 0,
            eBalanceCell2   = 1,
            eBalanceCell3   = 2,
            eBalanceCell4   = 3,
            eBalanceCell5   = 4,
            eBalanceCell6   = 5,
            eBalanceCell7   = 6,
            eBalanceCell8   = 7,
            eBalanceCell9   = 8,
            eBalanceCell10  = 9,
            eBalanceCell11  = 10,
            eBalanceCell12  = 11,
            eBalanceCell13  = 12,
            eBalanceCell14  = 13,
            eBalanceCell15  = 14,
            eBalanceLowest  = 20,
            eBalanceHightest= 21,

        };

		//-------------------------------------------//
		/// @fn run( )
		/// @brief short circuited a cell
		/// @param request : the cell requested to be short circuited (from enum eCell
		///          those value must be from enum tRun
		//-------------------------------------------//
        virtual void run( int request );

	//---------------------------- Protected Members -------------------------//
    protected:

        // Reference to the AFE object to request measure
        cAfe* m_afe;

};

#endif // TYVA_MEASURE_CBALANCE_HPP_
