/*!
 ------------------------------------------------------------------------------
                   TYVA ENERGIE Technical Software Department
  ------------------------------------------------------------------------------
  Copyright (c) 2013-2015, TYVA ENERGIE.
  Address.
  All rights reserved.
  This source program is the property of TYVA ENERGIE Company mentioned above
  and may not be copied in any form or by any means, whether in part or in whole,
  except under license expressly granted by such TYVA ENERGIE company.
  All copies of this program, whether in part or in whole, and
  whether modified or not, must display this and all other
  embedded copyright and ownership notices in full.
  ------------------------------------------------------------------------------
 
  Project : BMS

  ------------------------------------------------------------------------------
  */

/**
******************************************************************************
* @file      cBalance.cpp
* @author    ajouve
* @date      15 f�vr. 2019
* @brief     Cells balancing management
* @details   
******************************************************************************
*/


#include "cBalance.hpp"


//-------------------------------------------//
/// @fn cBalance( )
/// @brief cBalance constructor
//-------------------------------------------//
cBalance::cBalance() {
}

//-------------------------------------------//
/// @fn ~cBalance( )
/// @brief cBalance destructor
//-------------------------------------------//
cBalance::~cBalance() {
}


//-------------------------------------------//
/// @fn init( )
/// @brief to initialized object
/// @retval return 0 if all ok, -1 if error
//-------------------------------------------//
int cBalance::init( cAfe* afe ) {
    int ret(0);

    m_afe = afe;

    return ret;
}


//-------------------------------------------//
/// @fn run( )
/// @brief short circuited a cell
/// @param request : the cell requested to be short circuited (from enum eCell
//-------------------------------------------//
void cBalance::run(int request) {

    switch (request) {
        case eBalanceLowest:
            m_afe->balance(cAfe::eLowest);
            break;
        case eBalanceHightest:
            m_afe->balance(cAfe::eLowest);
            break;
        default:
            m_afe->balance((cAfe::tBalance)request);
            break;
    }
}




