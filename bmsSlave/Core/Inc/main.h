/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

#define MAX_NUMBER_OF_CELLS 15

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define VCELL1_Pin GPIO_PIN_0
#define VCELL1_GPIO_Port GPIOC
#define VCELL2_Pin GPIO_PIN_1
#define VCELL2_GPIO_Port GPIOC
#define VOLTAGE_PACK_Pin GPIO_PIN_2
#define VOLTAGE_PACK_GPIO_Port GPIOC
#define NTC0_Pin GPIO_PIN_3
#define NTC0_GPIO_Port GPIOC
#define NTC2_Pin GPIO_PIN_1
#define NTC2_GPIO_Port GPIOA
#define NTC3_Pin GPIO_PIN_3
#define NTC3_GPIO_Port GPIOA
#define NTC4_Pin GPIO_PIN_4
#define NTC4_GPIO_Port GPIOA
#define NTC5_Pin GPIO_PIN_5
#define NTC5_GPIO_Port GPIOA
#define NTC6_Pin GPIO_PIN_6
#define NTC6_GPIO_Port GPIOA
#define NTC7_Pin GPIO_PIN_7
#define NTC7_GPIO_Port GPIOA
#define NTC1_Pin GPIO_PIN_4
#define NTC1_GPIO_Port GPIOC
#define NTC8_Pin GPIO_PIN_0
#define NTC8_GPIO_Port GPIOB
#define P_BALANCE_CELL_1_Pin GPIO_PIN_13
#define P_BALANCE_CELL_1_GPIO_Port GPIOB
#define P_BALANCE_CELL_0_Pin GPIO_PIN_14
#define P_BALANCE_CELL_0_GPIO_Port GPIOB
#define P_ALERT_Pin GPIO_PIN_15
#define P_ALERT_GPIO_Port GPIOB
#define CMD_SOC_LED_Pin GPIO_PIN_7
#define CMD_SOC_LED_GPIO_Port GPIOC
#define CMD_STATUS_LED_Pin GPIO_PIN_8
#define CMD_STATUS_LED_GPIO_Port GPIOC
#define N_EN_CAN_Pin GPIO_PIN_8
#define N_EN_CAN_GPIO_Port GPIOA
#define AFE_SCL_Pin GPIO_PIN_9
#define AFE_SCL_GPIO_Port GPIOA
#define AFE_SDA_Pin GPIO_PIN_10
#define AFE_SDA_GPIO_Port GPIOA
#define WAKEUP_AFE_Pin GPIO_PIN_10
#define WAKEUP_AFE_GPIO_Port GPIOC
#define N_EN_P2V5REF_Pin GPIO_PIN_5
#define N_EN_P2V5REF_GPIO_Port GPIOB
#define N_EN_MEAS_VPACK_Pin GPIO_PIN_7
#define N_EN_MEAS_VPACK_GPIO_Port GPIOB
#define N_EN_VCC_NTC_Pin GPIO_PIN_8
#define N_EN_VCC_NTC_GPIO_Port GPIOB
#define N_EN_VCELL_Pin GPIO_PIN_9
#define N_EN_VCELL_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
