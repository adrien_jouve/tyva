#include "pch.h"

#include "tools/cCrcSoft.hpp"

#define TestCaseName CRC


//-------------------------------------------//
/// @brief test the CRC calculation
//-------------------------------------------//
TEST(TestCaseName, crcCalculation) {

	cCrcSoft crc;

	uint8_t testVal(0x4F);

	int ret = crc.run(&testVal, 1);

	EXPECT_EQ(ret, 0xEA);
}

//-------------------------------------------//
/// @brief test the CRC calculation
//-------------------------------------------//
TEST(TestCaseName, crcTest) {

	cCrcSoft crc;

	uint8_t testVal(0xFF);

	int ret = crc.run(&testVal, 1);

	EXPECT_EQ(ret, 0x4C);
}
