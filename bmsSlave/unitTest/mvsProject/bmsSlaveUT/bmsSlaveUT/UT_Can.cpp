#include "pch.h"

#include "Drivers/CAN/cCan.hpp"

#define TestCaseName CAN

class mockCan : public cCan {
public:
	mockCan(uint32_t masterAddress) :
		cCan(masterAddress)
	{
	};
	uint8_t m_data[20];
	uint32_t m_size;

	//-------------------------------------------//
	/// @fn transmit( )
	/// @brief call the parent method
	//-------------------------------------------//
	virtual int transmit(uint32_t *pData, uint32_t size) {
		return cCan::transmit(pData, size);
	};

	//-------------------------------------------//
	/// @fn transmit( )
	/// @brief stub this method to only save the data
	//-------------------------------------------//
	virtual int transmit(uint8_t *pData, uint32_t size) {
		m_size = size;

		if (size > 20) {
			printf("Size to high\n");
			return -1;
		}


		for (uint32_t  i = 0; i < size; i++, pData++) {
			m_data[i] = *pData;
		}
		return 0;
	};
};


//-------------------------------------------//
/// @brief use the canMock to get obtain the data
///  used to send to the transmit method
//-------------------------------------------//
TEST(TestCaseName, convertUint32ToUnit8Array) {

	mockCan can(0x00);
	uint32_t data[5] = { 0x00010203, 0x04050607, 0x08091011, 0x12131415, 0x16171819 };
	uint32_t expectedSize = 20;

	int ret = can.transmit(data, sizeof(data) / sizeof(*data));

	EXPECT_EQ(can.m_data[0], 0x0);
	EXPECT_EQ(can.m_data[1], 0x1);
	EXPECT_EQ(can.m_data[2], 0x2);
	EXPECT_EQ(can.m_data[3], 0x3);

	EXPECT_EQ(can.m_data[4], 0x4);
	EXPECT_EQ(can.m_data[5], 0x5);
	EXPECT_EQ(can.m_data[6], 0x6);
	EXPECT_EQ(can.m_data[7], 0x7);

	EXPECT_EQ(can.m_data[8], 0x8);
	EXPECT_EQ(can.m_data[9], 0x9);
	EXPECT_EQ(can.m_data[10], 0x10);
	EXPECT_EQ(can.m_data[11], 0x11);

	EXPECT_EQ(can.m_data[12], 0x12);
	EXPECT_EQ(can.m_data[13], 0x13);
	EXPECT_EQ(can.m_data[14], 0x14);
	EXPECT_EQ(can.m_data[15], 0x15);

	EXPECT_EQ(can.m_data[16], 0x16);
	EXPECT_EQ(can.m_data[17], 0x17);
	EXPECT_EQ(can.m_data[18], 0x18);
	EXPECT_EQ(can.m_data[19], 0x19);

	EXPECT_EQ(can.m_size, expectedSize);

	EXPECT_EQ(ret, 0);
}


//-------------------------------------------//
/// @brief use the canMock to get obtain the data
///  used to send to the transmit method
//-------------------------------------------//
TEST(TestCaseName, recursiveMethod) {
	
	cCan can(0x00);

	uint8_t data[5] = { 0x03, 0x07, 0x11, 0x15, 0x19 };

	int ret = can.transmit(data, sizeof(data) / sizeof(*data));

	EXPECT_EQ(ret, 0);
}