#include "stm32_HAL_stubs.h"


int HAL_GetTick(void) { return 1; }

void HAL_GPIO_Init(GPIO_TypeDef  *GPIOx, GPIO_InitTypeDef *GPIO_Init) {}

void HAL_GPIO_WritePin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, GPIO_PinState PinState) {}

void HAL_Delay(uint32_t Delay) {}

void Error_Handler(void) {}

