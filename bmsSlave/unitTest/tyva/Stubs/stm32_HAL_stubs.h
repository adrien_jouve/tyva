
#pragma once
#include "stm32_stubs.h"

#ifdef __cplusplus
extern "C" {
#endif

#define  GPIO_MODE_INPUT            (0x00000000u)   /*!< Input Floating Mode                   */
#define  GPIO_MODE_OUTPUT_PP        (0x00000001u)   /*!< Output Push Pull Mode                 */
#define  GPIO_PULLDOWN				(0x00000002u)   /*!< Pull-down activation                */
#define  GPIO_NOPULL				(0x00000000u)   /*!< No Pull-up or Pull-down activation  */
#define  GPIO_SPEED_FREQ_LOW        (0x00000000u)   /*!< range up to 5 MHz, please refer to the product datasheet */



typedef enum
{
	GPIO_PIN_RESET = 0U,
	GPIO_PIN_SET
}GPIO_PinState;

int HAL_GetTick(void);

void HAL_GPIO_Init(GPIO_TypeDef  *GPIOx, GPIO_InitTypeDef *GPIO_Init);

void HAL_GPIO_WritePin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, GPIO_PinState PinState);

void HAL_Delay(uint32_t Delay);

#ifdef __cplusplus
}
#endif