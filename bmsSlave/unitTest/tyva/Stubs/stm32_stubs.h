#pragma once

#include <stdint.h>

#define MAX_NUMBER_OF_CELLS 15

#ifdef __cplusplus
extern "C" {
#endif



typedef struct
{
	uint32_t StdId;    /*!< Specifies the standard identifier.
							This parameter must be a number between Min_Data = 0 and Max_Data = 0x7FF. */

	uint32_t ExtId;    /*!< Specifies the extended identifier.
							This parameter must be a number between Min_Data = 0 and Max_Data = 0x1FFFFFFF. */

	uint32_t IDE;      /*!< Specifies the type of identifier for the message that will be transmitted.
							This parameter can be a value of @ref CAN_identifier_type */

	uint32_t RTR;      /*!< Specifies the type of frame for the message that will be transmitted.
							This parameter can be a value of @ref CAN_remote_transmission_request */

	uint32_t DLC;      /*!< Specifies the length of the frame that will be transmitted.
							This parameter must be a number between Min_Data = 0 and Max_Data = 8. */

	int TransmitGlobalTime; /*!< Specifies whether the timestamp counter value captured on start
							of frame transmission, is sent in DATA6 and DATA7 replacing pData[6] and pData[7].
							@note: Time Triggered Communication Mode must be enabled.
							@note: DLC must be programmed as 8 bytes, in order these 2 bytes are sent.
							This parameter can be set to ENABLE or DISABLE. */

} CAN_TxHeaderTypeDef;

typedef struct
{
	uint32_t StdId;    /*!< Specifies the standard identifier.
							This parameter must be a number between Min_Data = 0 and Max_Data = 0x7FF. */

	uint32_t ExtId;    /*!< Specifies the extended identifier.
							This parameter must be a number between Min_Data = 0 and Max_Data = 0x1FFFFFFF. */

	uint32_t IDE;      /*!< Specifies the type of identifier for the message that will be transmitted.
							This parameter can be a value of @ref CAN_identifier_type */

	uint32_t RTR;      /*!< Specifies the type of frame for the message that will be transmitted.
							This parameter can be a value of @ref CAN_remote_transmission_request */

	uint32_t DLC;      /*!< Specifies the length of the frame that will be transmitted.
							This parameter must be a number between Min_Data = 0 and Max_Data = 8. */

	uint32_t Timestamp; /*!< Specifies the timestamp counter value captured on start of frame reception.
							@note: Time Triggered Communication Mode must be enabled.
							This parameter must be a number between Min_Data = 0 and Max_Data = 0xFFFF. */

	uint32_t FilterMatchIndex; /*!< Specifies the index of matching acceptance filter element.
							This parameter must be a number between Min_Data = 0 and Max_Data = 0xFF. */

} CAN_RxHeaderTypeDef;

typedef struct
{
	int MODER;       /*!< GPIO port mode register,               Address offset: 0x00      */
	int OTYPER;      /*!< GPIO port output type register,        Address offset: 0x04      */
	int OSPEEDR;     /*!< GPIO port output speed register,       Address offset: 0x08      */
	int PUPDR;       /*!< GPIO port pull-up/pull-down register,  Address offset: 0x0C      */
	int IDR;         /*!< GPIO port input data register,         Address offset: 0x10      */
	int ODR;         /*!< GPIO port output data register,        Address offset: 0x14      */
	int BSRR;        /*!< GPIO port bit set/reset  register,     Address offset: 0x18      */
	int LCKR;        /*!< GPIO port configuration lock register, Address offset: 0x1C      */
	int AFR[2];      /*!< GPIO alternate function registers,     Address offset: 0x20-0x24 */
	int BRR;         /*!< GPIO Bit Reset register,               Address offset: 0x28      */

} GPIO_TypeDef;

typedef struct
{
	uint32_t Pin;        /*!< Specifies the GPIO pins to be configured.
							 This parameter can be any value of @ref GPIO_pins */

	uint32_t Mode;       /*!< Specifies the operating mode for the selected pins.
							 This parameter can be a value of @ref GPIO_mode */

	uint32_t Pull;       /*!< Specifies the Pull-up or Pull-Down activation for the selected pins.
							 This parameter can be a value of @ref GPIO_pull */

	uint32_t Speed;      /*!< Specifies the speed for the selected pins.
							 This parameter can be a value of @ref GPIO_speed */

	uint32_t Alternate;  /*!< Peripheral to be connected to the selected pins
							  This parameter can be a value of @ref GPIOEx_Alternate_function_selection */
}GPIO_InitTypeDef;

void Error_Handler(void);

typedef enum
{
	HAL_OK = 0x00,
	HAL_ERROR = 0x01,
	HAL_BUSY = 0x02,
	HAL_TIMEOUT = 0x03
} HAL_StatusTypeDef;

typedef enum
{
	DISABLE = 0,
	ENABLE = !DISABLE
} FunctionalState;


#ifdef __cplusplus
}
#endif