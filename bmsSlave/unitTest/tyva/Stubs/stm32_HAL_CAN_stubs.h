#pragma once
#include "stm32_HAL_stubs.h"

#define CAN_FILTERMODE_IDMASK       (0x00000000U)  /*!< Identifier mask mode */
#define CAN_FILTERSCALE_32BIT       (0x00000001U)  /*!< One 32-bit filter  */
#define CAN_RX_FIFO0                (0x00000000U)  /*!< CAN receive FIFO 0 */

#define CAN_IT_RX_FIFO0_MSG_PENDING ((uint32_t)0x0)  /*!< FIFO 0 message pending interrupt */
#define CAN_RTR_DATA                (0x00000000U)  /*!< Data frame   */
#define CAN_ID_STD                  (0x00000000U)  /*!< Standard Id */


#ifdef __cplusplus
extern "C" {
#endif

uint32_t iterationCanStub;

typedef enum
{
	HAL_CAN_STATE_RESET = 0x00U,  /*!< CAN not yet initialized or disabled */
	HAL_CAN_STATE_READY = 0x01U,  /*!< CAN initialized and ready for use   */
	HAL_CAN_STATE_LISTENING = 0x02U,  /*!< CAN receive process is ongoing      */
	HAL_CAN_STATE_SLEEP_PENDING = 0x03U,  /*!< CAN sleep request is pending        */
	HAL_CAN_STATE_SLEEP_ACTIVE = 0x04U,  /*!< CAN sleep mode is active            */
	HAL_CAN_STATE_ERROR = 0x05U   /*!< CAN error state                     */

} HAL_CAN_StateTypeDef;

typedef struct
{
	uint32_t TIR;  /*!< CAN TX mailbox identifier register */
	uint32_t TDTR; /*!< CAN mailbox data length control and time stamp register */
	uint32_t TDLR; /*!< CAN mailbox data low register */
	uint32_t TDHR; /*!< CAN mailbox data high register */
} CAN_TxMailBox_TypeDef;

typedef struct
{
	uint32_t RIR;  /*!< CAN receive FIFO mailbox identifier register */
	uint32_t RDTR; /*!< CAN receive FIFO mailbox data length control and time stamp register */
	uint32_t RDLR; /*!< CAN receive FIFO mailbox data low register */
	uint32_t RDHR; /*!< CAN receive FIFO mailbox data high register */
} CAN_FIFOMailBox_TypeDef;

typedef struct
{
	uint32_t FR1; /*!< CAN Filter bank register 1 */
	uint32_t FR2; /*!< CAN Filter bank register 1 */
} CAN_FilterRegister_TypeDef;

typedef struct
{
	uint32_t FilterIdHigh;          /*!< Specifies the filter identification number (MSBs for a 32-bit
										 configuration, first one for a 16-bit configuration).
										 This parameter must be a number between Min_Data = 0x0000 and Max_Data = 0xFFFF. */

	uint32_t FilterIdLow;           /*!< Specifies the filter identification number (LSBs for a 32-bit
										 configuration, second one for a 16-bit configuration).
										 This parameter must be a number between Min_Data = 0x0000 and Max_Data = 0xFFFF. */

	uint32_t FilterMaskIdHigh;      /*!< Specifies the filter mask number or identification number,
										 according to the mode (MSBs for a 32-bit configuration,
										 first one for a 16-bit configuration).
										 This parameter must be a number between Min_Data = 0x0000 and Max_Data = 0xFFFF. */

	uint32_t FilterMaskIdLow;       /*!< Specifies the filter mask number or identification number,
										 according to the mode (LSBs for a 32-bit configuration,
										 second one for a 16-bit configuration).
										 This parameter must be a number between Min_Data = 0x0000 and Max_Data = 0xFFFF. */

	uint32_t FilterFIFOAssignment;  /*!< Specifies the FIFO (0 or 1U) which will be assigned to the filter.
										 This parameter can be a value of @ref CAN_filter_FIFO */

	uint32_t FilterBank;            /*!< Specifies the filter bank which will be initialized.
										 For single CAN instance(14 dedicated filter banks),
										 this parameter must be a number between Min_Data = 0 and Max_Data = 13.
										 For dual CAN instances(28 filter banks shared),
										 this parameter must be a number between Min_Data = 0 and Max_Data = 27. */

	uint32_t FilterMode;            /*!< Specifies the filter mode to be initialized.
										 This parameter can be a value of @ref CAN_filter_mode */

	uint32_t FilterScale;           /*!< Specifies the filter scale.
										 This parameter can be a value of @ref CAN_filter_scale */

	uint32_t FilterActivation;      /*!< Enable or disable the filter.
										 This parameter can be a value of @ref CAN_filter_activation */

	uint32_t SlaveStartFilterBank;  /*!< Select the start filter bank for the slave CAN instance.
										 For single CAN instances, this parameter is meaningless.
										 For dual CAN instances, all filter banks with lower index are assigned to master
										 CAN instance, whereas all filter banks with greater index are assigned to slave
										 CAN instance.
										 This parameter must be a number between Min_Data = 0 and Max_Data = 27. */

} CAN_FilterTypeDef;

typedef struct
{
	uint32_t Prescaler;                  /*!< Specifies the length of a time quantum.
											  This parameter must be a number between Min_Data = 1 and Max_Data = 1024. */

	uint32_t Mode;                       /*!< Specifies the CAN operating mode.
											  This parameter can be a value of @ref CAN_operating_mode */

	uint32_t SyncJumpWidth;              /*!< Specifies the maximum number of time quanta the CAN hardware
											  is allowed to lengthen or shorten a bit to perform resynchronization.
											  This parameter can be a value of @ref CAN_synchronisation_jump_width */

	uint32_t TimeSeg1;                   /*!< Specifies the number of time quanta in Bit Segment 1.
											  This parameter can be a value of @ref CAN_time_quantum_in_bit_segment_1 */

	uint32_t TimeSeg2;                   /*!< Specifies the number of time quanta in Bit Segment 2.
											  This parameter can be a value of @ref CAN_time_quantum_in_bit_segment_2 */

	FunctionalState TimeTriggeredMode;   /*!< Enable or disable the time triggered communication mode.
											  This parameter can be set to ENABLE or DISABLE. */

	FunctionalState AutoBusOff;          /*!< Enable or disable the automatic bus-off management.
											  This parameter can be set to ENABLE or DISABLE. */

	FunctionalState AutoWakeUp;          /*!< Enable or disable the automatic wake-up mode.
											  This parameter can be set to ENABLE or DISABLE. */

	FunctionalState AutoRetransmission;  /*!< Enable or disable the non-automatic retransmission mode.
											  This parameter can be set to ENABLE or DISABLE. */

	FunctionalState ReceiveFifoLocked;   /*!< Enable or disable the Receive FIFO Locked mode.
											  This parameter can be set to ENABLE or DISABLE. */

	FunctionalState TransmitFifoPriority;/*!< Enable or disable the transmit FIFO priority.
											  This parameter can be set to ENABLE or DISABLE. */

} CAN_InitTypeDef;

typedef struct
{
	uint32_t              MCR;                 /*!< CAN master control register,         Address offset: 0x00          */
	uint32_t              MSR;                 /*!< CAN master status register,          Address offset: 0x04          */
	uint32_t              TSR;                 /*!< CAN transmit status register,        Address offset: 0x08          */
	uint32_t              RF0R;                /*!< CAN receive FIFO 0 register,         Address offset: 0x0C          */
	uint32_t              RF1R;                /*!< CAN receive FIFO 1 register,         Address offset: 0x10          */
	uint32_t              IER;                 /*!< CAN interrupt enable register,       Address offset: 0x14          */
	uint32_t              ESR;                 /*!< CAN error status register,           Address offset: 0x18          */
	uint32_t              BTR;                 /*!< CAN bit timing register,             Address offset: 0x1C          */
	uint32_t                   RESERVED0[88];       /*!< Reserved, 0x020 - 0x17F                                            */
	CAN_TxMailBox_TypeDef      sTxMailBox[3];       /*!< CAN Tx MailBox,                      Address offset: 0x180 - 0x1AC */
	CAN_FIFOMailBox_TypeDef    sFIFOMailBox[2];     /*!< CAN FIFO MailBox,                    Address offset: 0x1B0 - 0x1CC */
	uint32_t                   RESERVED1[12];       /*!< Reserved, 0x1D0 - 0x1FF                                            */
	uint32_t              FMR;                 /*!< CAN filter master register,          Address offset: 0x200         */
	uint32_t              FM1R;                /*!< CAN filter mode register,            Address offset: 0x204         */
	uint32_t                   RESERVED2;           /*!< Reserved, 0x208                                                    */
	uint32_t              FS1R;                /*!< CAN filter scale register,           Address offset: 0x20C         */
	uint32_t                   RESERVED3;           /*!< Reserved, 0x210                                                    */
	uint32_t              FFA1R;               /*!< CAN filter FIFO assignment register, Address offset: 0x214         */
	uint32_t                   RESERVED4;           /*!< Reserved, 0x218                                                    */
	uint32_t              FA1R;                /*!< CAN filter activation register,      Address offset: 0x21C         */
	uint32_t                   RESERVED5[8];        /*!< Reserved, 0x220-0x23F                                              */
	CAN_FilterRegister_TypeDef sFilterRegister[28]; /*!< CAN Filter Register,                 Address offset: 0x240-0x31C   */
} CAN_TypeDef;

typedef struct __CAN_HandleTypeDef
{
	CAN_TypeDef                 *Instance;                 /*!< Register base address */

	CAN_InitTypeDef             Init;                      /*!< CAN required parameters */

	HAL_CAN_StateTypeDef   State;                     /*!< CAN communication state */

	uint32_t               ErrorCode;                 /*!< CAN Error code.*/

} CAN_HandleTypeDef;

CAN_HandleTypeDef hcan1;

HAL_StatusTypeDef HAL_CAN_Start(CAN_HandleTypeDef *hcan) { return HAL_OK; }

HAL_StatusTypeDef HAL_CAN_ActivateNotification(CAN_HandleTypeDef *hcan, uint32_t ActiveITs) { return HAL_OK; }

HAL_StatusTypeDef HAL_CAN_GetRxMessage(CAN_HandleTypeDef *hcan, uint32_t RxFifo, CAN_RxHeaderTypeDef *pHeader, uint8_t aData[]) { return HAL_OK; }

HAL_StatusTypeDef HAL_CAN_AddTxMessage(CAN_HandleTypeDef *hcan, CAN_TxHeaderTypeDef *pHeader, uint8_t aData[], uint32_t *pTxMailbox) { 
	if (iterationCanStub >= 3)
		return HAL_OK;
	else {
		iterationCanStub++;
		return HAL_TIMEOUT;
	}
}

uint32_t HAL_GetUIDw0(void) { return 1; }

HAL_StatusTypeDef HAL_CAN_ConfigFilter(CAN_HandleTypeDef *hcan, CAN_FilterTypeDef *sFilterConfig) { return HAL_OK; }

#ifdef __cplusplus
}
#endif