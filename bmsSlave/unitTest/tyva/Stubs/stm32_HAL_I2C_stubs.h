
#pragma once
#include "stm32_HAL_stubs.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
	uint32_t CR1;         /*!< I2C Control register 1,            Address offset: 0x00 */
	uint32_t CR2;         /*!< I2C Control register 2,            Address offset: 0x04 */
	uint32_t OAR1;        /*!< I2C Own address 1 register,        Address offset: 0x08 */
	uint32_t OAR2;        /*!< I2C Own address 2 register,        Address offset: 0x0C */
	uint32_t TIMINGR;     /*!< I2C Timing register,               Address offset: 0x10 */
	uint32_t TIMEOUTR;    /*!< I2C Timeout register,              Address offset: 0x14 */
	uint32_t ISR;         /*!< I2C Interrupt and status register, Address offset: 0x18 */
	uint32_t ICR;         /*!< I2C Interrupt clear register,      Address offset: 0x1C */
	uint32_t PECR;        /*!< I2C PEC register,                  Address offset: 0x20 */
	uint32_t RXDR;        /*!< I2C Receive data register,         Address offset: 0x24 */
	uint32_t TXDR;        /*!< I2C Transmit data register,        Address offset: 0x28 */
} I2C_TypeDef;

typedef struct
{
	uint32_t Timing;              /*!< Specifies the I2C_TIMINGR_register value.
									This parameter calculated by referring to I2C initialization
										   section in Reference manual */

	uint32_t OwnAddress1;         /*!< Specifies the first device own address.
									This parameter can be a 7-bit or 10-bit address. */

	uint32_t AddressingMode;      /*!< Specifies if 7-bit or 10-bit addressing mode is selected.
									This parameter can be a value of @ref I2C_ADDRESSING_MODE */

	uint32_t DualAddressMode;     /*!< Specifies if dual addressing mode is selected.
									This parameter can be a value of @ref I2C_DUAL_ADDRESSING_MODE */

	uint32_t OwnAddress2;         /*!< Specifies the second device own address if dual addressing mode is selected
									This parameter can be a 7-bit address. */

	uint32_t OwnAddress2Masks;    /*!< Specifies the acknowledge mask address second device own address if dual addressing mode is selected
									This parameter can be a value of @ref I2C_OWN_ADDRESS2_MASKS */

	uint32_t GeneralCallMode;     /*!< Specifies if general call mode is selected.
									This parameter can be a value of @ref I2C_GENERAL_CALL_ADDRESSING_MODE */

	uint32_t NoStretchMode;       /*!< Specifies if nostretch mode is selected.
									This parameter can be a value of @ref I2C_NOSTRETCH_MODE */

} I2C_InitTypeDef;

typedef struct
{
	uint32_t CCR;         /*!< DMA channel x configuration register        */
	uint32_t CNDTR;       /*!< DMA channel x number of data register       */
	uint32_t CPAR;        /*!< DMA channel x peripheral address register   */
	uint32_t CMAR;        /*!< DMA channel x memory address register       */
} DMA_Channel_TypeDef;

typedef struct
{
	uint32_t Request;                   /*!< Specifies the request selected for the specified channel.
											 This parameter can be a value of @ref DMA_request */

	uint32_t Direction;                 /*!< Specifies if the data will be transferred from memory to peripheral,
											 from memory to memory or from peripheral to memory.
											 This parameter can be a value of @ref DMA_Data_transfer_direction */

	uint32_t PeriphInc;                 /*!< Specifies whether the Peripheral address register should be incremented or not.
											 This parameter can be a value of @ref DMA_Peripheral_incremented_mode */

	uint32_t MemInc;                    /*!< Specifies whether the memory address register should be incremented or not.
											 This parameter can be a value of @ref DMA_Memory_incremented_mode */

	uint32_t PeriphDataAlignment;       /*!< Specifies the Peripheral data width.
											 This parameter can be a value of @ref DMA_Peripheral_data_size */

	uint32_t MemDataAlignment;          /*!< Specifies the Memory data width.
											 This parameter can be a value of @ref DMA_Memory_data_size */

	uint32_t Mode;                      /*!< Specifies the operation mode of the DMAy Channelx.
											 This parameter can be a value of @ref DMA_mode
											 @note The circular buffer mode cannot be used if the memory-to-memory
												   data transfer is configured on the selected Channel */

	uint32_t Priority;                  /*!< Specifies the software priority for the DMAy Channelx.
											 This parameter can be a value of @ref DMA_Priority_level */
} DMA_InitTypeDef;

typedef enum
{
	HAL_UNLOCKED = 0x00,
	HAL_LOCKED = 0x01
} HAL_LockTypeDef;

typedef enum
{
	HAL_DMA_STATE_RESET = 0x00U,  /*!< DMA not yet initialized or disabled    */
	HAL_DMA_STATE_READY = 0x01U,  /*!< DMA initialized and ready for use      */
	HAL_DMA_STATE_BUSY = 0x02U,  /*!< DMA process is ongoing                 */
	HAL_DMA_STATE_TIMEOUT = 0x03U,  /*!< DMA timeout state                     */
}HAL_DMA_StateTypeDef;

typedef struct
{
	uint32_t ISR;         /*!< DMA interrupt status register,                 Address offset: 0x00 */
	uint32_t IFCR;        /*!< DMA interrupt flag clear register,             Address offset: 0x04 */
} DMA_TypeDef;

typedef struct __DMA_HandleTypeDef
{
	DMA_Channel_TypeDef    *Instance;                                                  /*!< Register base address                */

	DMA_InitTypeDef       Init;                                                        /*!< DMA communication parameters         */

	HAL_LockTypeDef       Lock;                                                        /*!< DMA locking object                   */

	HAL_DMA_StateTypeDef  State;                                                  /*!< DMA transfer state                   */

	void                  *Parent;                                                     /*!< Parent object state                  */

	void(*XferCpltCallback)(struct __DMA_HandleTypeDef * hdma);     /*!< DMA transfer complete callback       */

	void(*XferHalfCpltCallback)(struct __DMA_HandleTypeDef * hdma); /*!< DMA Half transfer complete callback  */

	void(*XferErrorCallback)(struct __DMA_HandleTypeDef * hdma);    /*!< DMA transfer error callback          */

	void(*XferAbortCallback)(struct __DMA_HandleTypeDef * hdma);   /*!< DMA transfer abort callback          */

	uint32_t          ErrorCode;                                                  /*!< DMA Error code                       */

	DMA_TypeDef            *DmaBaseAddress;                                            /*!< DMA Channel Base Address             */

	uint32_t               ChannelIndex;                                               /*!< DMA Channel Index                    */

}DMA_HandleTypeDef;

typedef enum
{
	HAL_I2C_STATE_RESET = 0x00U,   /*!< Peripheral is not yet Initialized         */
	HAL_I2C_STATE_READY = 0x20U,   /*!< Peripheral Initialized and ready for use  */
	HAL_I2C_STATE_BUSY = 0x24U,   /*!< An internal process is ongoing            */
	HAL_I2C_STATE_BUSY_TX = 0x21U,   /*!< Data Transmission process is ongoing      */
	HAL_I2C_STATE_BUSY_RX = 0x22U,   /*!< Data Reception process is ongoing         */
	HAL_I2C_STATE_LISTEN = 0x28U,   /*!< Address Listen Mode is ongoing            */
	HAL_I2C_STATE_BUSY_TX_LISTEN = 0x29U,   /*!< Address Listen Mode and Data Transmission
												   process is ongoing                         */
	HAL_I2C_STATE_BUSY_RX_LISTEN = 0x2AU,   /*!< Address Listen Mode and Data Reception
												   process is ongoing                         */
	HAL_I2C_STATE_ABORT = 0x60U,   /*!< Abort user request ongoing                */
	HAL_I2C_STATE_TIMEOUT = 0xA0U,   /*!< Timeout state                             */
	HAL_I2C_STATE_ERROR = 0xE0U    /*!< Error                                     */

} HAL_I2C_StateTypeDef;

typedef enum
{
	HAL_I2C_MODE_NONE = 0x00U,   /*!< No I2C communication on going             */
	HAL_I2C_MODE_MASTER = 0x10U,   /*!< I2C communication is in Master Mode       */
	HAL_I2C_MODE_SLAVE = 0x20U,   /*!< I2C communication is in Slave Mode        */
	HAL_I2C_MODE_MEM = 0x40U    /*!< I2C communication is in Memory Mode       */

} HAL_I2C_ModeTypeDef;

typedef struct __I2C_HandleTypeDef
{
	I2C_TypeDef                *Instance;      /*!< I2C registers base address                */

	I2C_InitTypeDef            Init;           /*!< I2C communication parameters              */

	uint8_t                    *pBuffPtr;      /*!< Pointer to I2C transfer buffer            */

	uint16_t                   XferSize;       /*!< I2C transfer size                         */

	uint16_t              XferCount;      /*!< I2C transfer counter                      */

	uint32_t              XferOptions;    /*!< I2C sequantial transfer options, this parameter can
													be a value of @ref I2C_XFEROPTIONS */

	uint32_t              PreviousState;  /*!< I2C communication Previous state          */

	HAL_StatusTypeDef(*XferISR)(struct __I2C_HandleTypeDef *hi2c, uint32_t ITFlags, uint32_t ITSources);  /*!< I2C transfer IRQ handler function pointer */

	DMA_HandleTypeDef          *hdmatx;        /*!< I2C Tx DMA handle parameters              */

	DMA_HandleTypeDef          *hdmarx;        /*!< I2C Rx DMA handle parameters              */

	HAL_LockTypeDef            Lock;           /*!< I2C locking object                        */

	HAL_I2C_StateTypeDef  State;          /*!< I2C communication state                   */

	HAL_I2C_ModeTypeDef   Mode;           /*!< I2C communication mode                    */

	uint32_t              ErrorCode;      /*!< I2C Error code                            */

	uint32_t              AddrEventCount; /*!< I2C Address Event counter                 */

} I2C_HandleTypeDef;

I2C_HandleTypeDef hi2c1;

HAL_StatusTypeDef HAL_I2C_Master_Transmit(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData, uint16_t Size, uint32_t Timeout) { return HAL_OK; }

HAL_StatusTypeDef HAL_I2C_Master_Receive(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData, uint16_t Size, uint32_t Timeout) { return HAL_OK; }

#ifdef __cplusplus
}
#endif